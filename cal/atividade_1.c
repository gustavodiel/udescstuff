#include <stdio.h>
#include <stdlib.h>

#define NUM_MAX 1000000000000

unsigned long int resultados[NUM_MAX];

int main(){
    unsigned long int n;

    printf("Quantos numeros deseja digitar?\n\t");
    scanf("%lu", &n);

    unsigned long int what;

    while(n--){
        printf("\t");
        scanf("%lu", &what);
        resultados[what]++;
    }

    unsigned long int res = 0;
    while(res != -1){
        printf("Deseja obter a informacao de que numero? (-1 para encerrar)\n\t");
        scanf("%lu", &res);
        if (res != -1){
            printf("O numero %lu apareceu %lu vezes\n", res, resultados[res]);
        }
    }


}
