#include <iostream>
#include <vector>

using namespace std;

std::vector<int> comprimentos;

int qntBlocos(int n){
    if (n == 0){
        return 0;
    }
    cout << "oii";
    int menor = INT_MAX;
    for (int i = 0; i < comprimentos.size(); ++i){
        int d = 1 + qntBlocos(n - comprimentos[i]);
        if (d < menor){
            menor = d;
        }
    }
    return menor;
}

int main(){
   int t;
   cin >> t;
   while (t--){
       int n, m;
       cin >> n >> m;

       while (n--){
           int a;
           cin >> a;
           comprimentos.push_back(a);
       }

       cout << qntBlocos(comprimentos.size());
   }
    return 0;
}
