#include <stdio.h>
#include <math.h>

#define ERRO_MINIMO 0.00000001

double raiz(double n, double start, double end){
    double comp = (start + end)/2.0;
    double sub = comp * comp - n;
    if (fabs(sub) < ERRO_MINIMO ){
        return comp;
    }
    return raiz(n, sub > 0 ? start : comp, sub > 0 ? comp : end );
}

int main(){
    printf("Qual numero voce deseja saber a raiz?\n\t");
    double n;
    scanf("%lf", &n);

    printf("A raiz de %f é %f\n", n, raiz(n, 0, n));
    return 0;
}
