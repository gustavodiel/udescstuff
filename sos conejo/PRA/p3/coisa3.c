#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "coisa3.h"

int main () {
	FILE * pFile,
		 * pFileNew;

	char * src,
		 * dst,
		 * buffer;

	char linha[68];
	size_t result;

	int i=0,
		j=0,
		aux=0, 
		inclui,
		cont = 0,
		contLi = 0,
		contPart = 0,
		contPart_Max[9];


	while(i<9){
		contPart_Max[i] = 0;
		i++;
	}
	pFile = fopen ( "routes.dat.txt" , "r");
	if (pFile==NULL) {
		fputs ("File error\n\n",stderr);
		exit (1);
	}
	system("clear");
	
	  // allocate memory to contain the whole file:
	buffer = (char*) malloc (sizeof(char)*MBuffer+1);
	if (buffer == NULL) {
		fputs ("Memory error\n\n",stderr);
		exit (2);
	}

	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	  // start pt1
	  // armazena tamanhos máximos de cada campo
	
	while ((result = fread (buffer, sizeof(char), MBuffer, pFile))!=EOF){
		buffer[result] = '\0';
		if (!result)
			break; //printf("\nAcabou a leitura do arquivo\n");
		
		for(i = 0; i < result; i++){		
			if (buffer[i] != '\n'){
				if (buffer[i] == ','){
					if(contPart > contPart_Max[j])
						contPart_Max[j] = contPart;
					j++;
					contPart=0;
				}else{
					contPart++;
				}
			}else{
				if(contPart > contPart_Max[8] && j==8){
						contPart_Max[8] = contPart-1;
				}

				contLi++;
				j=0;
				contPart=0;				
			}
		}
	}
	  
	  // terminate pt1
	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	  // start pt2
	  // cria novo arquivo com tamanhos definidos

	rewind(pFile);
	pFileNew = fopen ( "routes.dat_New.txt" , "w");
	if (pFileNew==NULL) {
		fputs ("File error\n\n",stderr);
		exit (1);
	}

	contLi = 0;
	while ((result = fread (buffer, sizeof(char), MBuffer, pFile))!=EOF){
		buffer[result] = '\0';
		if (!result)
			break; //printf("\nAcabou a criação do arquivo routes.dat_New\n");
		
		for(i = 0; i < result; i++){
		    if(buffer[i] != '\r') {
	            if (buffer[i] == '\n'){
	            	for(aux=contPart;aux<contPart_Max[j];aux++){
				     	linha[cont] = ' ';
				     	cont++;
	            	}
				    linha[cont] = '\n';
				    fputs(linha, pFileNew); //fprintf(pFileNew, "%s", linha);
				    cont=0;
                    contLi++;
		            j=0;
		            contPart=0;	
                    }
                 else {
		            if (buffer[i] == ','){
				        for(aux=contPart;aux<contPart_Max[j];aux++){
					        linha[cont] = ' ';
					        cont++;
					    }
		             		linha[cont] = ',';
		             		cont++;
			            	j++;
				            contPart=0;
			        }else{
				        linha[cont] = buffer[i];
				        cont++;
		         		contPart++;
		         	}
		        }
		    }
		}
	}
	  // terminate pt2
	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	  // start pt3
	  //Seleciona campos source e destino e cria indice
	fclose(pFileNew);
	
	pFileNew = fopen ("routes.dat_New.txt", "r");
	if(!pFileNew){
		fputs ("File error\n\n",stderr);
		exit (1);	
	}
	
	src = (char*) malloc (sizeof(char)*contPart_Max[2]);
	dst = (char*) malloc (sizeof(char)*contPart_Max[4]);
	if (src == NULL || dst == NULL) {
		fputs ("Memory error\n\n",stderr);
		exit (2);
	}

	//cria arvore
	arvore a;
	noh *novo;
	novo = malloc(sizeof &novo);
	a = malloc(sizeof(arvore));
	a->esq = a->dir = NULL;
	novo->dir = novo->esq = NULL;

	//define valores var aux
	j=contLi=0;
	inclui = N;
	while ((result = fread (buffer, sizeof(char), MBuffer, pFileNew))!=EOF){
		if (!result){
			break; //printf("\nAcabou a leitura do arquivo\n");
		}
		for(i = 0; i < result; i++){
		    if(buffer[i] != '\r') {
		    	if (buffer[i] == '\n'){
	                contLi++;
		            j=contPart=aux=0;

		            if(inclui == S){
		            	novo->chave = atoi(src);
		            	insereL(atoi(dst), &novo->conteudo);
		            	insereA(a, novo);
		            }
		            inclui = N;
	            }else{
		            if (buffer[i] == ','){
		            	j++;
		            	aux=0;
			            contPart=0;
			        }else{
			        	if((j==3) | (j==5) | (j==7)){
			        		//se for um dos campos qe queremos
							switch (j){
								case 3:
									//pega o src
									src[aux] = buffer[i];
					    			aux++;
					    			break;
					    		case 5:
					    			//pega o dst
					    			dst[aux] = buffer[i];
					    			aux++;
					    			break;
					    		case 7:
					    			if(buffer[i] == '0'){ //verifica se n tem escalas
					    				inclui = S;
					    				if(contLi==4723+1) printf("%i - %s %s\n",contLi+1, src, dst);
					    			}
					         		break;
					         }
	   					}
		         		contPart++;
		    		}
		    	}
		    }
			
		}
	}	

	fclose(pFile);
	fclose(pFileNew);
	free (buffer);
	return 0;
}	

//Funções celula
//site: http://www.ime.usp.br/~pf/algoritmos/aulas/binst.html

void imprimeL(celula *le){
	if(le!=NULL){
		printf("%d\n", le->conteudo);
		imprimeL(le->prox);
	}
}

celula * buscaL(int x, celula *le){
	celula *p;
	p = le;
	while(p != NULL && p->conteudo != x)
		p = p->prox;
	return p;
}

void insereL(int x, celula *p){
	celula *nova;
	nova = malloc(sizeof(celula));
	nova -> conteudo = x;
	nova -> prox = p->prox;
	p->prox = nova;
}


void removeL(celula *p){
	celula *lixo;
	lixo = p->prox;
	p->prox = lixo->prox;
	free(lixo);
}

//Funções árvore
//site: http://www.ime.usp.br/~pf/algoritmos/aulas/binst.html

// Recebe uma árvore de busca r e
// um número k. Devolve um nó
// cuja chave é k; se tal nó não existe,
// devolve NULL.
arvore buscaA (arvore r, int k){
    if (r == NULL || r->chave == k)
    	return r;

    if (r->chave > k)
       return buscaA (r->esq, k);
    else
       return buscaA (r->dir, k);
}

// A função recebe uma árvore de busca r
// e uma folha avulsa novo e insere a folha
// na árvore de modo que a árvore continue
// sendo de busca. A função devolve a raiz 
// da árvore resultante.
noh *insereA (arvore r, noh *novo){ 
    if (r == NULL){
    	return novo;
    }
    printf("%i\n",novo->chave);
    if (r->chave > novo->chave)
       r->esq = insereA (r->esq, novo);
    else{
       r->dir = insereA (r->dir, novo);
    }
    return r;
}

// Recebe uma árvore não vazia r.
// Remove a raiz da árvore e rearranja
// a árvore de modo que ela continue sendo
// de busca. Devolve o endereço
// da nova raiz.
arvore removeraizA (arvore r){
    noh *p, *q;
    if (r->esq == NULL) {
       q = r->dir;
       free (r);
       return q;
    }
    p = r; q = r->esq;
    while (q->dir != NULL) {
       p = q; q = q->dir;
    }
    // q é nó anterior a r na ordem e-r-d
    // p é pai de q
    if (p != r) {
       p->dir = q->esq;
       q->esq = r->esq;
    }
    q->dir = r->dir;
    free (r);
    return q;
}
