//Compile : gcc -Wall -o prog coisa3.c coisa3.h
#define MBuffer 8192
#define S 1
#define N 0

/*	3. Faça um programa que a partir do nome do aeroporto (Sao Paulo-Congonhas (CGH), Brazil) de origem (Source airport) indique todos aeroportos de destino possíveis (Destination airport) com nenhuma escala (Stops).
		a) Considere que para o problema acima deve ser criado um índice em memória, ou seja, na carga do programa o arquivo é lido inteiro e é criado na memória uma árvore binária (o campo chave do índice é source)
		b) Persista o índice em disco
		c) Altere o programa para criar o índice apenas se ele não existir no disco
*/

//celula
typedef struct regL{
	int conteudo;
	struct regL *prox;
} regL;

typedef regL celula;

//Árvore
//-------------------structs
typedef struct reg {
   	int chave;
   	celula conteudo;
   struct reg *esq, *dir; 
} noh; /* nó */

typedef noh *arvore; /* árvore */

//----------------protótipos
arvore buscaA (arvore r,int k);
noh *insereA (arvore r, noh *novo);
arvore removeraizA (arvore r);

void imprimeL(celula *le);
celula * buscaL(int x, celula *le);
void insereL(int x, celula *p);
void removeL(celula *p);