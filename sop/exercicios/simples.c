#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_THREADS    5

void *PrintHello(void *arg) {
   long tid = (long)arg;
   printf("Alo da thread %ld\n",
          tid);
   pthread_exit(tid * tid * tid * tid);
}

int main (int argc, char *argv[]) {
   pthread_t threads[NUM_THREADS];
   int rc;
   long t;
   long total = 0;
   for (t=0; t<NUM_THREADS; t++){
      printf("main: criando thread %ld\n", t);
      rc = pthread_create(&threads[t],
                          NULL,
                          PrintHello,
                          (void *)t);
      void* result;
      pthread_join(threads[t], &result);
      printf("Recebeu: %d\n", (long)result);
      total += (long)result;
      if (rc) {
         printf("ERRO - rc=%d\n", rc);
         exit(-1);
      }
   }
   printf("O total deu %d\n", total);
   /* Ultima coisa que main() deve fazer */
   pthread_exit(NULL);
}
