#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>


#define T_PAR 1
#define T_IMPAR 2

#define NELEM 2000000


typedef struct arg {
    char tipo;
    int *vet;
} argumentos;

int conta(void* ptr)
{

    argumentos* args = (argumentos*)ptr;


     int i, total = 0;

     for (i = 0; i < NELEM; i++) {
	  if ((args->tipo == T_PAR) && ((args->vet[i] % 2) == 0))
	      total++;
	  else if ((args->tipo == T_IMPAR) && ((args->vet[i] % 2) != 0))
	       total++;
     }
     return total;
}

int main(int argc, char *argv[])
{
     int vetor[NELEM], i, pares, impares, rc;
     struct timeval tv_ini, tv_fim;
     unsigned long time_diff, sec_diff, usec_diff, msec_diff;

     srandom(time(NULL));
     for (i = 0; i < NELEM; i++) {
	  vetor[i] = (int)random();
/*	  vetor[i] = i*2;*/
     }

     /* marca o tempo de inicio */
     rc = gettimeofday(&tv_ini, NULL);
     if (rc != 0) {
	  perror("erro em gettimeofday()");
	  exit(1);
     }

     argumentos *argPares, *argImpares;
     argPares = malloc(sizeof(argumentos));
     argImpares = malloc(sizeof(argumentos));
     argPares->tipo = T_PAR;
     argPares->vet = vetor;

     argImpares->tipo = T_IMPAR;
     argImpares->vet = vetor;

     /* faz o processamento de interesse */
    pthread_t tPares, tImpares;
    pthread_create(&tPares, NULL, (void*) conta, (void*)argPares);
    pthread_create(&tImpares, NULL, (void*)conta, (void*)argImpares);
     //pares = conta(vetor, T_PAR);
     //impares = conta(vetor, T_IMPAR);
    void* resPares;
    void* resImpares;

    /* pega os retornos */
    pthread_join(tPares, &resPares);
    pthread_join(tImpares, &resImpares);

    pares = (long)resPares;
    impares = (long)resImpares;

     /* marca o tempo de final */
     rc = gettimeofday(&tv_fim, NULL);
     if (rc != 0) {
	  perror("erro em gettimeofday()");
	  exit(1);
     }
     /* calcula a diferenca entre os tempos, em usec */
     time_diff = (1000000L*tv_fim.tv_sec + tv_fim.tv_usec) -
  	         (1000000L*tv_ini.tv_sec + tv_ini.tv_usec);
     /* converte para segundos + microsegundos (parte fracionária) */
     sec_diff = time_diff / 1000000L;
     usec_diff = time_diff % 1000000L;

     /* converte para msec */
     msec_diff = time_diff / 1000;

     printf("O vetor tem %d numeros pares e %d numeros impares.\n", pares,
	    impares);
/*     printf("Tempo de execucao: %lu.%06lu seg\n", sec_diff, usec_diff);*/
     printf("Tempo de execucao: %lu.%03lu mseg\n", msec_diff, usec_diff%1000);
     return 0;
}
