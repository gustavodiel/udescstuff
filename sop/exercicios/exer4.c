#include <stdio.h>
#include <pthread.h>

#define NUM (1L<<30)
#define N 4
#define A_CONTAR NUM/N

unsigned long int total = 0;

void* calcula(void* vN){
    long meuN = (long)vN;
    unsigned long int res = 0;
    while(meuN--){
        total++;
    }
    pthread_exit((void*)res);
}

int main(){

    pthread_t threads[N];
    int i;
    unsigned long int aContar = NUM/N;
    printf("%lld\n", aContar);
    for (i = 0; i < N; i++){
        pthread_create(&threads[i], NULL, (void*)calcula, (void*) aContar);
    }

    for ( i = 0; i < N; i++ ){
        pthread_join(threads[i], NULL);
    }
    printf("Temos um total de %lld\n", total);
    return 0;

}
