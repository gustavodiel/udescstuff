/**
 * Criacao de processos:
 * Unix - 0 parametros ( fork() )
 * Win - 10 parametros ( Create Process()  )
 * VM: - 12 parametros ( SYS%CREPRC  )
 *
 * Criacao de Thread:
 * Pthread: pthread_create( pthread* thread, pthread_attr_t *attr, void*(*start_routine) (void*), void* args )
 */

#include <pthread.h>
#include <stdio.h>

#define NUM_THREAD 5

void *PrintHello(void* arg){
    long tid = (long)arg;
    printf("Alo de %ld\n", tid);
    pthread_exit(NULL);
}

int main(){
    int i;
    for (i = 0; i < NUM_THREAD; i++){
        printf("Criando thread %d\n", i);
        pthread_t thread;
        pthread_create(&thread, NULL, (void*)PrintHello, (void *)i);
    }

    return 0;
}
