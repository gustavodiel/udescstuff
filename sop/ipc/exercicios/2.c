#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#define MAX 2000

int n, vai = 1;
pthread_mutex_t mtx;
pthread_cond_t cf2, cf3;

void f1(void* args){
    pthread_mutex_lock(&mtx);
    n = n * 16;
    printf("%s\n", __PRETTY_FUNCTION__);
    vai = 3;
    pthread_cond_signal(&cf3);
    pthread_mutex_unlock(&mtx);
}
void f2(void* args){
    while(vai != 2)
        pthread_cond_wait(&cf2, &mtx);
    pthread_mutex_lock(&mtx);
    n = n / 7;
    printf("%s\n", __PRETTY_FUNCTION__);
    pthread_mutex_unlock(&mtx);
}
void f3(void* args){
    while(vai != 3)
        pthread_cond_wait(&cf3, &mtx);
    pthread_mutex_lock(&mtx);
    n = n + 40;
    vai = 2;
    printf("%s\n", __PRETTY_FUNCTION__);
    pthread_cond_signal(&cf2);
    pthread_mutex_unlock(&mtx);
}

int main(){
    pthread_t t1, t2, t3;
    int rc;
    n = 1;
    pthread_mutex_init(&mtx, NULL);
    rc = pthread_create(&t1, NULL, (void*)f1, NULL);
    rc = pthread_create(&t2, NULL, (void*)f2, NULL);
    rc = pthread_create(&t3, NULL, (void*)f3, NULL);
    rc = pthread_join(t1, NULL);
    rc = pthread_join(t2, NULL);
    rc = pthread_join(t3, NULL);
    pthread_mutex_destroy(&mtx);
    pthread_cond_destroy(&cf2);
    pthread_cond_destroy(&cf3);
    printf("%d\n", n);
    return 0;
}
