#include <stdio.h>
#include <pthread.h>
/*
 * \brief Executa uma thread simples
 */
void * func(){
    int i = 0;
    for (i = 0; i < 100; ++i){
        printf("\r%d from %s on %s at %s\n", i, __PRETTY_FUNCTION__, __DATE__, __TIME__);
    }

    pthread_exit(NULL);
}


int main(int argc, char* args[]){
    printf("%s\n", args[1]);
    pthread_t um, dois;
    pthread_create(&um, NULL, func, NULL);
    pthread_create(&dois, NULL, func, NULL);

    pthread_join(um, NULL);
    pthread_join(dois, NULL);
    return 0;
}
