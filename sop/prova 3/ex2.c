#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>

char* getTipo(int t){
    switch (t){
        case 0:
            return "Desconhecido";
        case 1:
            return "FIFO File";
        case 2:
            return "Character Dev";
        case 4:
            return "Directory";
        case 6:
            return "Block Dev";
        case 8:
            return "Regular File";
        case 10:
            return "Link file";
        case 12:
        case 14:
        default:
            return "Nao sei";
    }
}

int main(int argc, char *argv[]){
    if (argc != 2){
        printf("O senhor(a) esqueceu do diretorio!\nBurro\n");
        return 1;
    }

    DIR* ptrDir = opendir(argv[1]);
    if (!ptrDir){
        printf("Mano, o diretorio non ecxiste!\n");
        return 1;
    }

    struct dirent* strDir;
    printf("\033[1m%-7s%-17s%-45s%-10s\033[0m\n", "Tipo", "Tipo em String", "Nome", "Tamanho");
    while((strDir = readdir(ptrDir))){
        printf("\x1b[31m%-7d\x1b[32m%-17s\x1b[36m%-45s\x1b[32m%-10d\n", strDir->d_type, getTipo(strDir->d_type), strDir->d_name, strDir->d_reclen);
    }

    closedir(ptrDir);

    return 0;
}
