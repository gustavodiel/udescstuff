#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]){
    if (argc != 4){
        printf("O senhor(a) esqueceu do arquivo de entrada, de saida e o tamanho do Buffer\
                !\nBurro\n");
        return 1;
    }
    int desc = open(argv[1], O_RDONLY);
    if (desc == -1){
        printf("Provavelmente arquivo non ecxiste!\n");
        return 2;
    }
    int desc2 = open(argv[2], O_WRONLY);
    if (desc2 == -1){
        printf("Deu MER** ao abrir o arquivo! Tentando Gambi Method!\n");
        desc2 = open(argv[2], O_CREAT | O_WRONLY);
        if (desc2 == -1){
            printf("Metudus Gambis nao funcionou, abortando!\n");
            return 3;
        }
    }
    int bufferSize = atoi(argv[3]);
    char buffer[bufferSize];
    int numRead;
    while ((numRead = read(desc, &buffer, bufferSize))){
        write(desc2, buffer, numRead);
    }

    close(desc);
    close(desc2);

    return 0;
}
