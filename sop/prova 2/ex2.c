#include <stdio.h>
#include <stdlib.h>

/*
 * Teste:
 *32KB = 262144 bits - Fisica
 *64KB = 524288 bits - Logica
 *       12 bits     - Tamanho Deslocamento
 *
 *XXXXYYYY
 *Y = Deslocamento
 *X = Indice
 *
 */

int main(int argc, char* argv[]){
    if (argc < 4){
        printf("Execucao do arquivo: ./exercicio tamFisico tamLogico bitDesloc\n");
        return 0;
    }
    int tamEnderecoFisico  =     atoi(argv[1]);
    int tamEnderecoLogico  =     atoi(argv[2]);
    int bitDesloc          =     atoi(argv[3]);

    int bitsPNumeroPagina = tamEnderecoLogico - bitDesloc;
    int bitsPNumeroMoldura = tamEnderecoFisico - bitDesloc;

    /* TAMANHO DA PAGINA */
    int tamPagina = 2<<(bitDesloc-1);
    int qntPaginas= 2<<(bitsPNumeroPagina-1);
    int qntMolduras=2<<(bitsPNumeroMoldura-1);

    /* Tamanho de Endereçamento  de Memoria Fisica */
    int tamEMF = tamPagina * qntPaginas;
    int tamEML = tamPagina * qntMolduras;

    /* TAMANHO DA TABLEA DE PAGINAS PARA 4 bits DE CONTROLE */
    int tamTabela = (bitsPNumeroPagina + 4) * qntPaginas;

    printf( "Numero de paginas: %d\nNumero de molduras: %d\nTamanho das paginas: %d\n", qntPaginas, qntMolduras, tamPagina );
    printf( "Tamanho Enderecamento Fisico: %d\nTamanho Enderecamento Logico: %d\n", tamEMF, tamEML );
    printf( "A tabela possui: %d\n", tamTabela );

    printf( "Por favor querido usuario, digite os enderecos virtuais ( tamanho de %d bits, em decimal :) )\n", tamEnderecoLogico );

    int num;
    while ((scanf("%d", &num))){
        printf("NUM: %d\n", num);
    }


    return 0;
}
