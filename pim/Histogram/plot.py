    from PIL import Image
import numpy as np
import time as tm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from pylab import *

pil1=Image.open('image.png')
(l,h)=pil1.size
print(l,h)
out=Image.new(pil1.mode, (l,h))
a=pil1.split()


tabelaRed = []
tabelaGreen = []
tabelaBlue = []
tab = []
for i in range(256):
    tabelaRed.append(0)
    tabelaGreen.append(0)
    tabelaBlue.append(0)
    tab.append(i)

maior = 0
for j in range(0, h):
    for i in range(0, l):
        pix = pil1.getpixel((i,j))
        tabelaRed[pix[0]] = tabelaRed[pix[0]] + 1
        #tabelaGreen[pix[1]] = tabelaGreen[pix[1]] + 1
        #tabelaBlue[pix[2]] = tabelaBlue[pix[2]] + 1
        if tabelaBlue[pix[0]] > maior:
            maior = tabelaBlue[pix[0]]
        if tabelaGreen[pix[0]] > maior:
            maior = tabelaGreen[pix[0]]
        if tabelaRed[pix[0]] > maior:
            maior = tabelaRed[pix[0]]

n, bins, patches = plt.hist(tabelaRed, 256, facecolor='red', alpha=0.75)



plt.axis([0, 255, 0, maior])
plt.show()
