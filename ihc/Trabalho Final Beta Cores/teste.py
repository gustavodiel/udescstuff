import time
import struct

from color_searcher import ColorNames

import Quartz.CoreGraphics as CG

from pynput.mouse import Button, Controller

class ScreenPixel(object):
    """Captures the screen using CoreGraphics, and provides access to
    the pixel values.
    """

    def capture(self, region = None):

        if region is None:
            region = CG.CGRectInfinite
        else:
            # TODO: Odd widths cause the image to warp. This is likely
            # caused by offset calculation in ScreenPixel.pixel, and
            # could could modified to allow odd-widths
            if region.size.width % 2 > 0:
                emsg = "Capture region width should be even (was %s)" % (
                    region.size.width)
                raise ValueError(emsg)

        # Create screenshot as CGImage
        image = CG.CGWindowListCreateImage(
            region,
            CG.kCGWindowListOptionOnScreenOnly,
            CG.kCGNullWindowID,
            CG.kCGWindowImageDefault)

        # Intermediate step, get pixel data as CGDataProvider
        prov = CG.CGImageGetDataProvider(image)

        # Copy data out of CGDataProvider, becomes string of bytes
        self._data = CG.CGDataProviderCopyData(prov)

        # Get width/height of image
        self.width = CG.CGImageGetWidth(image)
        self.height = CG.CGImageGetHeight(image)

    def pixel(self, x, y):
        """Get pixel value at given (x,y) screen coordinates

        Must call capture first.
        """

        # Pixel data is unsigned char (8bit unsigned integer),
        # and there are for (blue,green,red,alpha)
        data_format = "BBBB"

        # Calculate offset, based on
        # http://www.markj.net/iphone-uiimage-pixel-color/
        offset = 4 * ((self.width*int(round(y))) + int(round(x)))

        # Unpack data from string into Python'y integers
        b, g, r, a = struct.unpack_from(data_format, self._data, offset=offset)

        # Return BGRA as RGBA
        return (r, g, b)


if __name__ == '__main__':
    # Timer helper-function
    import contextlib

    mouse = Controller()


    @contextlib.contextmanager
    def timer(msg):
        start = time.time()
        yield
        end = time.time()
        print ("%s: %.02fms" % (msg, (end-start)*1000))


    # Example usage
    sp = ScreenPixel()

    lastCor =  ""
    lastTom = ""
    curr = ""
    number = ""
    tom = ""
    while True:

        sp.capture(region=CG.CGRectMake(mouse.position[0], mouse.position[1], 2, 2))
        color = sp.pixel(0, 0)

        #with timer("GetColor"):
        ret = ColorNames.findNearestPortugueseSimpleColorName(color).split(":")

        tom = "Normal"

        if len(ret) == 2:
            tom = ""
            (curr, number) = ret
        else:
            (curr, tom, number) = ret

        if curr != lastCor or tom != lastTom:
            print(curr, tom)
            lastCor = curr
            lastTom = tom



