/* Como Compilar */
/*
 * Windows: gcc -o boneco.exe hello.c -lopengl32 -lglut32
 * Linux:  gcc -o boneco boneco.c -lGL -lGLU -lglut
 * macOS: gcc -o boneco boneco.c -framework OpenGL -framework GLUT
 */
/* Basic Include */
#include <stdio.h>
#include <string.h>
#include <math.h>

/* OpenGL Include */
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

/* DEFINES */
#define WINDOW_HEIGHT 1920
#define WINDOW_WIDTH 1080

float angle=0.0;
float deltaAngle = 0.0;
float ratio;
float x=0.0f;
float y=1.5f;
float z=5.0f;
float lx=0.0f;
float ly=0.0f;
float lz=-1.0f;

int deltaMove = 0;
int h;
int w;
int frame;
int time;
int timebase=0;

char s[30];

static GLint listaBoneco;



void initWindow();

void reshapeFunc(int w1, int h1){
	if(h1 == 0)
		h1 = 1;

	w = w1;
	h = h1;
	ratio = 1.0f * w / h;
	// Reset the coordinate system before modifying
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the clipping volume
	gluPerspective(45,ratio,0.1,1000);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(x, y, z,x + lx,y + ly,z + lz,0.0f,1.0f,0.0f);

}




void montaBoneco() {
	glColor3f(1.0f, 1.0f, 1.0f);

// Corpao
	glTranslatef(0.0f ,0.7f, 0.0f);
		glutSolidSphere(0.6f,200,200);


// Cabeca
	glTranslatef(0.0f, 0.8f, 0.0f);
		glutSolidSphere(0.3f,100,100);

// Bracos
	glPushMatrix();
		glColor3f(0.5f, 0.1, 0);
			glTranslatef(0, -0.4f, 0.0f);
				glScalef(1, 0.1, 0.1);
					glTranslatef(0.5f, 0.f, 0.0f);
						glutSolidCube(1);
						glTranslatef(-1.0f, 0.f, 0.0f);
							glutSolidCube(1);
	glPopMatrix();

// Olhos
	glPushMatrix();
		glColor3f(0.0f,0.0f,0.0f);
			glTranslatef(0.05f, 0.10f, 0.25f);
				glutSolidSphere(0.05f,50,50);
				glTranslatef(-0.1f, 0.0f, 0.0f);
					glutSolidSphere(0.05f,50,50);
	glPopMatrix();

// Nariz
	glColor3f(1.0f, 0.5f , 0.5f);
		glRotatef(0.0f,1.0f, 0.0f, 0.0f);
			glutSolidCone(0.08f,0.5f,100,2);
}



GLuint criarBonecosNeve() {
	GLuint snowManDL = glGenLists(2);

	glNewList(snowManDL+1,GL_COMPILE);
		montaBoneco();
	glEndList();

	glNewList(snowManDL,GL_COMPILE);

	for(int i = 0; i < 1; i++)
		for(int j=0; j < 1; j++) {
			glPushMatrix();
			glTranslatef(i*10.0, 0, j * 10.0);
			glCallList(snowManDL + 1);
			glPopMatrix();
		}
	glEndList();

	return(snowManDL);
}

void initScene() {

	glEnable(GL_DEPTH_TEST);
	listaBoneco = criarBonecosNeve();

}

void orientMe(float ang) {
	lx = sin(ang);
	lz = -cos(ang);
	glLoadIdentity();
	gluLookAt(x, y, z,x + lx,y + ly,z + lz,0.0f,1.0f,0.0f);
}


void moveMeFlat(int i) {
	x = x + i*(lx)*0.1;
	z = z + i*(lz)*0.1;
	glLoadIdentity();
	gluLookAt(x, y, z, x + lx,y + ly,z + lz, 0.0f,1.0f,0.0f);
}


void renderScene(void) {

	if (deltaMove)
		moveMeFlat(deltaMove);
	if (deltaAngle) {
		angle += deltaAngle;
		orientMe(angle);
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Draw ground

	glColor3f(0.9f, 0.9f, 0.9f);
	glBegin(GL_QUADS);
		glVertex3f(-100.0f, 0.0f, -100.0f);
		glVertex3f(-100.0f, 0.0f,  100.0f);
		glVertex3f( 100.0f, 0.0f,  100.0f);
		glVertex3f( 100.0f, 0.0f, -100.0f);
	glEnd();

	// Draw 36 Snow Men

	glCallList(listaBoneco);

	frame++;
	time=glutGet(GLUT_ELAPSED_TIME);
	if (time - timebase > 1000) {
		sprintf(s,"FPS:%4.2f",frame*1000.0/(time-timebase));
		timebase = time;
		frame = 0;
	}

	glColor3f(0.0f,1.0f,1.0f);

	// Prepara View
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, w, 0, h);
	glScalef(1, -1, 1);
	glTranslatef(0, -h, 0);
	glMatrixMode(GL_MODELVIEW);

	glPushMatrix();
	glLoadIdentity();
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	glutSwapBuffers();
}

void processNormalKeys(unsigned char key, int x, int y) {

	if (key == 27)
		exit(0);
}

void pressKey(int key, int x, int y) {
	switch (key) {
		case GLUT_KEY_LEFT:
			deltaAngle = -0.01f;
			break;
		case GLUT_KEY_RIGHT:
			deltaAngle = 0.01f;
			break;
		case GLUT_KEY_UP:
			deltaMove = 1;
			break;
		case GLUT_KEY_DOWN:
			deltaMove = -1;
			break;

	}

}

void releaseKey(int key, int x, int y) {
	switch (key) {
		case GLUT_KEY_LEFT:
			if (deltaAngle < 0.0f)
				deltaAngle = 0.0f;
			break;
		case GLUT_KEY_RIGHT:
			if (deltaAngle > 0.0f)
				deltaAngle = 0.0f;
			break;
		case GLUT_KEY_UP:
			if (deltaMove > 0)
				deltaMove = 0;
			break;
		case GLUT_KEY_DOWN:
			if (deltaMove < 0)
				deltaMove = 0;
			break;
	}
}

void initWindow() {
	glutIgnoreKeyRepeat(1);

	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(pressKey);
	glutSpecialUpFunc(releaseKey);

	glutDisplayFunc(renderScene);

	glutIdleFunc(renderScene);

	glutReshapeFunc(reshapeFunc);

	initScene();

}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0,0);
	glutInitWindowSize(WINDOW_HEIGHT, WINDOW_WIDTH);
	glutCreateWindow("Trabalho Complementar 2 - Gustavo Diel");

	 // register all callbacks
	initWindow();
	glutMainLoop();
	return(0);
}
