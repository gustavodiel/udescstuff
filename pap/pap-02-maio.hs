{-

	Monada IO

Class Monada:

class Monad
	(>>=) :: m a -> (a -> m b) -> m b
	(>>) :: m a -> m b -> m b
	return :: a -> m a

class Read
	read :: String -> a

class Show
	show :: a -> String

-}
fat 0 = 1
fat n = n * fat (n - 1)

ola = do 	putStr "Qual o seu nome?\n"
         	nome <- getLine
         	putStr ("Ola " ++ nome ++ "\n")

ola' = putStr "Qual o seu nome\n" >> getLine >>=(\nome->putStr ("Ola " ++ nome ++ "\n"))

main = do  putStr "Fatorial de que numero?\n"
           n <- getLine
           let fato = fat (read n)
           putStr ("Fatoria de " ++ n ++ ": " ++ show ( fato ) ++ "\n")

main2 = do  putStr "Arquivo:\n"
            arq <- getLine
            txt <- readFile arq
            putStr txt