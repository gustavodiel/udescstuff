
import Data.List
import Data.Char

type Doc = String
type Line = String
type Word = String
{-
	repeat i: Cria lista infinita só de i
	toUpper xs: transofrma em Maiusculo
	takeWhile: recebe um valor e testa se e vdd ou falso
	fst (x, y): retorna x
	snd (x, y): retorn y
-}

{-
		Solucao do professor:

allNumWords [] = []
allNumWords ((i,l):xs) = zip (repeat i) (words l) ++ allNumWords xs

sortLs xs = sortBy cmpWord xs
	where cmpWord (_,w) (_,i) = compare ( map toUpper w) (map toUpper i)

amalga [] = []
amalga xxs@(x:xs) = ( map fst (takeWhile eqw xxs), snd x):amalga (dropWhile eqw xs)
	where eqw (_,w) = map toUpper w == map toUpper (snd x)

makeIndex txt = amalga ( sortLs(allNumWords(lines(txt))))
-- ou
-- makeIndex txt = amalga.sortList.allNumWords(lines txt)

-}

excluiCE [] = []
excluiCE ((x, xs):ys) = (x,eNormal xs):excluiCE ys
	where
		eNormal [] = []
		eNormal (z:zs) = if isAlpha z then z:eNormal zs else eNormal zs


enumLin xs = zip [1..]  (lines xs)

coisa a xs = (a, xs)

enumWords [] = []
enumWords ((x, xs):ys) = map (coisa x) (words xs) ++ enumWords ys
	where 

comparador (_,xs) (_,ys) = if (xs > ys) then GT else if (xs < ys) then LT else EQ

sortLs xs = sortBy comparador xs 

almalgamate ((x, xs):ys)  = x

co p [] = []
co p ((x, xs):ys) = if ( p == xs) then x:co p ys else []

alma [] = []
alma ((x, y):ys) = ( x:co y ys, y):alma ys

keep p [] = []
keep p ((x, xs):ys) = if ( p /= xs ) then (x, xs):ys else keep p ys

shorten [] = []
shorten ((x, y):ys) = (x,y):(shorten (keep y ys)) -- :shorten (keep y ys)

tudo xs = excluiCE (shorten (alma (sortLs (enumWords (enumLin (xs))))))
