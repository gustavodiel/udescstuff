{-
    Operadores lógicos
        & | ~ -> <->
    Dado:
        data Expr = E Expr Expr | Ou Expr Expr | Nao Expr | Bi Expr Expr | Imp Expr Expr
                    | Var String | C Bool
    Exemplo:
        ~ x1 ^ x2 === E (Nao (Var x1) (Var x2))
        ~ ( x1 ^ x2 ) === Nao (E (Var x1) (Var x2))
-}
{-
Recebe uma String -> Expr
-}

{-
exprLog => ||FT/
          &&FT/
          vazio
    F  => ->FT/
          <->FT/
          F'
    F' => ~F'/
          Q
    Q => var/
          (exprLog)
-}
import Data.Char
import Control.Monad
import qualified Data.Map as Map
import Text.ParserCombinators.Parsec
import Data.Either
import Data.Maybe

data Expr = E Expr Expr 
            | Ou Expr Expr 
            | Nao Expr 
            | BiImp Expr Expr 
            | Imp Expr Expr
            | Var String 
            | C Bool deriving Show

main = do {
        putStr "\nDigite a expressao";
        ex <- getLine;
        case avaliarExpr ex of
            Left err -> putStr ( err ++ "\n")
            Right r  -> putStr ( r ++ "\n")}
        

ret v1 Nothing = v1
ret v1 (Just (t, v2)) = t v1 v2

avaliarExpr e = parse expr "Erro:" e

-- Especificacao sintatica
expr = do {v1 <- term;  -- E -> TE'
            e <- expr';
            return (ret v1 e)}
       
expr' = do {string "||"; -- E' -> ||TE'
            v1 <- expr2';
            e <- expr';
            return (Just ("Ou", ret v1 e))} 
        <|>
        do {string "&&"; -- E' -> &&TE'
            v1 <- expr2';
            e <- expr';
            return (Just ("E", ret v1 e))}
        <|> return Nothing -- E' -> vazio 

expr2' = do {string "->"; -- E' -> ->TE'
            v1 <- term;
            e <- expr';
            return (Just ("Imp", ret v1 e))}
        <|>
        do {string "<->"; -- E' -> <->TE'
            v1 <- term;
            e <- expr';
            return (Just ("BiImp", ret v1 e))}
        <|> 
        do {v1 <- term; return (v1)}

term = do {char '~'; -- T' -> ~T'
            v1 <- term;
            return (Just ("Ou", ret v1))} 
        <|>
        do {v1 <- fator'; -- T' -> F'
            return (ret v1)}

fator' = var -- F -> numero 
        <|> do {char '('; e <- expr; char ')'; return []} -- F -> (E)
        
var = char 'V' <|> char 'F'