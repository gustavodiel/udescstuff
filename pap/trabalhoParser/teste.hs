import Data.Char
import Control.Monad
import qualified Data.Map as Map
import Text.ParserCombinators.Parsec
import Data.Either
import Data.Maybe

avaliarExpr e = parse exprA "Erro:" e

ret v1 Nothing = v1
ret v1 (Just (op, v2)) = op v1 v2 

exprA = do c <- termA
           e <- exprA'
           return ( ret c e)


exprA' = do {char '+'; -- E' -> +TE'
            v1 <- termA;
            e <- exprA';
            return (Just ((+), ret v1 e))} 
        <|>
        do {char '-'; -- E' -> -TE'
            v1 <- termA;
            e <- exprA';
            return (Just ((-), ret v1 e))}
        <|> return Nothing -- E' -> vazio

ident :: Parser String
ident = do c <- letter <|> char '_'
           cs <- many (letter <|> digit <|> char '_')
           return (c:cs)
      <?> "identifier"


termA = do
        v1 <- fatorA
        e <- termA'
        return (ret v1 e)
        
      
termA' = do {char '*'; -- T' -> *FT'
            v1 <- fatorA;
            e <- termA';
            return (Just ((*), ret v1 e))} 
        <|>
        do {char '/'; -- T' -> /FT'
            v1 <- fatorA;
            e <- termA';
            return (Just ((/), ret v1 e))}
        <|> return Nothing -- T' -> vazio

fatorA :: Parser String
fatorA = num -- F -> numero 
        <|> do {char '('; e <- exprA; char ')'; return e} -- F -> (E)


main = do {putStr "\nExpressao:";
          e <- getLine;
          case avaliarExpr e of
            Left err -> putStr ((show err)++ "\n")
            Right r  -> putStr ((show r) ++ "\n")}


{----------------------------------------------------------------------------------------------}
-- Especificacao lexica
num = decimal 

decimal 
    = do digits <- many1 digit;
         let n = foldl (\x d -> 10*x + toInteger (digitToInt d)) 0 digits
         return (fromIntegral n)
    



