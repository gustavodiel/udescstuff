{-
1.Todo nó é vermelho ou preto
2.Toda folha é preta
3.Se um nó é vermelho, seus filhos sao pretos
4.Sempre que for inserir um nó novo, vai ser vermelho
5.Raiz sempre é preta
     |3| ex1                |2|
    /   \                  /   \
 |1|      d             |1|    |3|
/  \             =>    /   \   /  \
a   |2|               a    b   c   d 
   /   \
   b    c
-}
module Main where


data Cor = V | P deriving Show
data Arvore a = No a Cor (Arvore a) (Arvore a) | Folha deriving Show

rot (No x3 P (No x1 V a (No x2 V b c)) d) = No x2 V (No x1 P a b) (No x3 P c d) -- ex1 la em cima :v
rot (No x3 P (No x2 V (No x1 V a b) c) d) = No x2 V (No x1 P a b) (No x3 P c d)
rot (No x1 P a (No x2 V b (No x3 V c d))) = No x2 V (No x1 P a b) (No x3 P c d)
rot (No x1 P a (No x3 V (No x2 V b c) d)) = No x2 V (No x1 P a b) (No x3 P c d)
rot a = a

insl e Folha = No e V Folha Folha
insl e a@(No e1 c esq dir)
    | e < e1 = rot (No e1 c (insl e esq) dir)
    | e > e1 = rot (No e1 c esq (insl e dir))    
    | e == e1 = a

insB e a = No e1 P esq dir
    where (No e1 c esq dir) = insl e a

ins e a = let (No e1 c esq dir) = insl e a in 
    No e1 P esq dir
{- Cria uma arvore de 1-}
arv = foldr ins Folha (reverse "Strogonoff de Frango da minha casa com batata palha e molho de creme de leite")
main = putStrLn "Hello World"