import Data.List (insertBy, foldl', sortBy)
import Data.Maybe (fromJust)
import Data.Ord (comparing)
import qualified Data.Map as M 

data Huffman = Folha Char Int | No Int Huffman Huffman deriving Eq

instance Show (Huffman) where
  show =
      go ""
    where
      spaces = map (const ' ')
      paren s = "(" ++ s ++ ")"
      go ss (Folha s o) = "--" ++ paren (show o) ++ show s ++ "\n"
      go ss (No o l r) =
          let root  = "--" ++ paren (show o) ++ "-+"
              ss' = ss ++ tail (spaces root)
              lbranch = go (ss' ++ "|") l
              rbranch = go (ss' ++ " ") r
          in root ++ lbranch
                  ++ ss' ++ "|\n"
                  ++ ss' ++ "`"
                  ++ rbranch


peso (Folha _ i) = i
peso (No i _ _) = i

junta xs ys = No (peso xs + peso ys) xs ys


freqChar _ [] = 0
freqChar y (x:xs) = if (y == x) then 1 + freqChar y xs else freqChar y xs

freq [] = []
freq (x:xs) = Folha x (freqChar x (x:xs)):freq xs

keep _ [] = []
keep y ((Folha x z):xs) = if y /= x then Folha x z:keep y xs else keep y xs

keepFolha _ [] = []
keepFolha y (x:xs) = if (y == x) then xs else keepFolha y xs

tiraIguais [] = []
tiraIguais ((Folha x y):xs) = Folha x y:tiraIguais (keep x xs)

extraiMin [Folha x y] = y
extraiMin ((Folha x y):xs) = if (y < (extraiMin xs)) then y else extraiMin xs

busca f ((Folha x y):xs) = if f == y then Folha x y else busca f xs


verSeVazio xs = xs == []

constroi xs = cria xs
  where
    cria [t] = t
    cria (ta:tb:ts) = cria (insertBy (comparing peso) (junta ta tb) ts)

codes =
    M.fromList . go []
  where
    -- leaf nodes mark the end of a path to a symbol
    go p (Folha s _) = [(s,reverse p)]
    -- traverse both branches and accumulate a reverse path
    go p (No _ l r) = go (0:p) l ++ go (1:p) r

encode tbl =
    concatMap get
  where
    get x = fromJust (M.lookup x tbl)



--codHuff :: Huffman -> [(Char, String)]

--compactar :: String -> Huffman -> String

--descompactar :: String -> Huffman -> String

--constroi :: [Huffman] -> Huffman




tudo xs = constroi (tiraIguais (freq xs))
