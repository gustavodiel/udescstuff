import Data.List
import Data.Ord
import Data.Function
import qualified Data.Binary.Put as P
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Internal as I

data Huffman = -- A data foi alterada para que fosse possivel fazer o instance Ord de um jeito bem simples
    Folha { c :: Char, i :: Int } |
    No { i :: Int, esq :: Huffman, dir :: Huffman } 
    deriving (Eq)

-- Aqui fazemos uma instancia de Show para mostrarmos a arvore bem bonitinha.
-- ( A beleza do resultado disse é inversamente proporcional a beleza desse codigo :v)
instance Show (Huffman) where
    show = imprime ""
        where
            bps = map (const ' ')
            paren s = "(" ++ s ++ ")"
            imprime ss (Folha s o) =    "-"
                ++ paren (show o) 
                ++ show s 
                ++ "\n"
            imprime ss (No o l r) =
                let raiz  = "-" ++ paren (show o) ++ "+"
                    ss' = ss ++ tail (bps raiz)
                    esq = imprime (ss' ++ "|") l
                    dir = imprime (ss' ++ " ") r
                in raiz ++ esq
                        ++ ss' ++ "|\n"
                        ++ ss' ++ "`"
                        ++ dir

-- Instace do Ord para que possamos comparar os Nos e Folhas
-- Como na data Huffman definimos o "nome" dos elementos de cada tipo de arvore
-- Podemos dizer que quando formos comparar uma arvore, usamos o elemento 'i', que
-- No nosso caso, é o inteiro
instance Ord Huffman where
    compare = compare `on` i

-- Calcula o peso de cada Folha
peso :: Huffman -> Int
peso (Folha _ i) = i
peso (No i _ _) = i

-- Junta duas Folhas em um No
junta :: Huffman -> Huffman -> Huffman 
junta xs ys = No (peso xs + peso ys) xs ys

-- Calcula as frequencias e cria Folhas com cada uma
freq :: String -> [Huffman] 
freq [] = []
freq (x:xs) = Folha x (freqChar x (x:xs)):freq xs
    where
        freqChar _ [] = 0
        freqChar y (x:xs) = if (y == x) then 1 + freqChar y xs else freqChar y xs

-- Tira todos as tuplas repetidas
tiraIguais :: [Huffman] -> [Huffman] 
tiraIguais [] = []
tiraIguais ((Folha x y):xs) = Folha x y:tiraIguais (keep x xs)
    where
        keep _ [] = []
        keep y ((Folha x z):xs) = if y /= x then Folha x z:keep y xs else keep y xs

-- Cria a arvore principal ( por causa da implementacao,
-- ele retorna uma lista de arvores, porem so possui um elemento)
constroi :: [Huffman] -> [Huffman] 
constroi (min1:min2:rest) = constroi lista
    where
        lista
            | length rest /= 0  = quickSort ((junta min1 min2):rest)
            | otherwise         = [junta min1 min2]
                where
                    quickSort [] = []
                    quickSort (x:xs) = l1 ++ [x] ++ l2
                        where
                            l1 = quickSort [y | y <- xs, y < x]
                            l2 = quickSort [y | y <- xs, y >= x]
constroi x = x

-- Pega o valor binario de cada caracter
codHuff :: Huffman -> [(Char, String)] 
codHuff (No _ esq dir)      = (codHuff' "0" esq) ++ (codHuff' "1" dir)
    where
        codHuff' n (No a esq dir)   = (codHuff' (n++"0") esq) ++ (codHuff' (n++"1") dir)
        codHuff' n (Folha a s)      = [(a, n)]

-- Converte uma String, seguindo uma arvore Huffman, numa cadeia de 0 e 1
compacta :: String -> Huffman -> String 
compacta [] _ = []
compacta (x:xs) ns = (getCode x (codHuff ns)) ++ compacta xs ns
    where
        getCode x v = if (x == (fst . head) v) then snd . head $ v else getCode x ((delete . head) v v) -- (delete (head v) v)

-- Desconverte de uma cadeia de 0 e 1 para a string original
descompacta :: String -> Huffman -> String 
descompacta xs tree = descompacta' tree xs
    where
        descompacta' (Folha c _) []          = [c]
        descompacta' (Folha c _) ys          = c : descompacta' tree ys
        descompacta' (No _ esq dir) (y:ys)   = descompacta' (if y == '0' then esq else dir) ys

-- usado como nossa main :v
arvore xs = head . constroi . tiraIguais . freq $ xs
listas xs = codHuff . arvore $ xs   -- Ou usamos (codHuff . arvore) xs
                                    -- ou codHuff ( arvore xs) :v
cod xs ar = compacta xs ar
dcd xs ar = descompacta xs ar 

-- Sarvar em Binary
-- qntCaracteresDiferentes   qntTotalCaracteres
-- listaTuplasCaracEFrequencia
-- palavraCompactada
-- lixo

tiposCara [] = 0
tiposCara (x:xs) = 1 + tiposCara (filter (/=x) xs)

freq' [] = [] 
freq' (x:xs) = (x, length (filter (==x) xs) +1):freq' (filter (/=x) xs)

salva = do
    putStr "Digite o nome do arquivo para salvar:  "
    arqSalva <- getLine
    putStr "Digite o arquivo .txt:  "
    arqLe <- getLine
    strSalva <- readFile arqLe
    let compa = compacta strSalva (arvore strSalva)
    let listaChars = freq' strSalva
    putStrLn compa
    let bTopo = P.runPut (junto  (tiposCara strSalva)  (length compa) listaChars compa)
    putStrLn (descompacta compa (arvore strSalva))
    L.writeFile arqSalva bTopo

junto n a xs ys = do
    put' n a
    put xs
    putStri ys

put' qntChars tiposChar  = do
    P.putWord32be (toEnum qntChars)
    P.putWord32be (toEnum tiposChar)

put [] = P.flush
put ((c, f):xs) = do
    P.putWord8 (I.c2w c)
    P.putWord32be (toEnum f)
    put xs


putStri [] = P.flush
putStri xs = do
    put8 (take 8 (xs ++ repeat '0'))
    putStri (drop 8 xs)

put8 [] = P.flush
put8 (x:xs) = do
    P.putWord8 (I.c2w x)
    put8 xs

readCoisa = do
    putStr "Digite o nome do arquivo para ler:  "
    arq <- getLine
    L.readFile arq




