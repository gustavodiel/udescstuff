{-
					Classes de Tipo

		Um tipo pertence a uma determinada classe:

Usado:
	class Show a where
		show: a-> String

-}


data Semana = Dom | Seg | Ter | Qua | Qui | Sex | Sab

{- Implementamos a nossa própria instancia de Show 😀💙💙💙😀-}
instance Show Semana where
	show Dom = "Domingo"
	show Seg = "Segunda"
	show Ter = "Terca"
	show Qua = "Quarta"
	show Qui = "Quinta"
	show Sex = "Sexta"
	show Sab = "Sabado"

{- <=[ Exercicio ]=> -}

{- Teste Para Arvore -}
data Cor = V | P deriving Show
data Arvore a = No a Cor (Arvore a) (Arvore a) | Folha

instance (Show a) => Show (Arvore a) where
	show Folha = "Folha"
	show (No x c esq dir) = "(" ++ show esq ++ ") " ++ show x ++ " " ++ show c ++ " (" ++ show dir ++ ")"


rot (No x3 P (No x1 V a (No x2 V b c)) d) = No x2 V (No x1 P a b) (No x3 P c d) -- ex1 la em cima 😚
rot (No x3 P (No x2 V (No x1 V a b) c) d) = No x2 V (No x1 P a b) (No x3 P c d)
rot (No x1 P a (No x2 V b (No x3 V c d))) = No x2 V (No x1 P a b) (No x3 P c d)
rot (No x1 P a (No x3 V (No x2 V b c) d)) = No x2 V (No x1 P a b) (No x3 P c d)
rot a = a

insl e Folha = No e V Folha Folha
insl e a@(No e1 c esq dir)
    | e < e1 = rot (No e1 c (insl e esq) dir)
    | e > e1 = rot (No e1 c esq (insl e dir))    
    | e == e1 = a

insB e a = No e1 P esq dir
    where (No e1 c esq dir) = insl e a

ins e a = let (No e1 c esq dir) = insl e a in 
    No e1 P esq dir
{- Cria uma arvore de 1 -}
arv = foldr ins Folha [1..10]

{- Classe Ja existentes (Como foram implementadas) 😇 -}

{- 
Escopos:

class Eq a where
	(==):: a -> a -> Bool
	(/=):: a -> a -> Bool

class (Eq a) => Ord a where
	compare :: a -> a -> Ordering

data Ordering = EQ | LT | GT
-}

{- Criacao de classe -}

class Len a where
	len:: a->Int

instance Len [a] where
	len [] = 0
	len (x:xs) = 1 + len xs

instance Len (Arvore a) where
	len Folha = 0
	len (No x c esq dir) = 1 + len esq + len dir

class Map f where
	map'::(a->b) -> f a -> f b

instance Map [] where
	map' = map

instance Map Arvore where
	map' f Folha = Folha
	map' f (No x c esq dir) = No (f x) c (map' f esq) (map' f dir)

--ink::(Map f, Num b) => f b -> f b

ink xs = map' (+1) xs

