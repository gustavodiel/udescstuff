
data Texto = Coisa String


instance Show Texto where
    show = imprime ""
        where
            imprime ns (Coisa xs) = show "Yoo: " ++ show xs

cT :: String -> Texto
cT xs = Coisa xs