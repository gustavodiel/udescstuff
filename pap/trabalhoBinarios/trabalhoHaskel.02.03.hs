inverte (x:[]) = [x]
inverte (x:xs) = inverte xs ++ [x]

ePoli (x:xs) = ( [x] ++ xs )  == inverte ( [x] ++ xs )

contem n (x:[]) = n == x
contem n (x:xs) = if n==x then True else contem n xs

interc (x:[]) ys = if contem x ys then [x] else []
interc (x:xs) ys = if contem x ys then x : interc xs ys else interc xs ys

repe (x:[]) = False
repe (x:xs) = if contem x xs then True else repe xs

menor (x:[]) = x
menor (x:xs) = if x < menor xs then x else menor xs

exclui _ [] = []
exclui n (x:xs) = if n == x then exclui n xs else x : exclui n xs

ordena [] = []
ordena (x:xs) = ordena menorV ++ (x : ordena maiorV)
   where menorV = [y | y <- xs, y <= x]
         maiorV = [y | y <- xs, y > x]


zipW _ _ [] = []
zipW _ [] _ = []
zipW f (x:xs) (y:ys) = f x y : zipW f xs ys

fibs' = 0:(1:zipW (+) fibs' (tail fibs'))
