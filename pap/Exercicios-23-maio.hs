import qualified Data.Binary.Put as P
import qualified Data.Binary.Get as G
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Internal as I

data Huffman = -- A data foi alterada para que fosse possivel fazer o instance Ord de um jeito bem simples
    Folha { c :: Char, i :: Int } |
    No { i :: Int, esq :: Huffman, dir :: Huffman } 
    deriving (Eq)

getTudo = do
    qntChar <- getNum
    qntTotalPalavra <- getNum
    tuplas <- getRegs qntChar
    palavra <- getTexts qntTotalPalavra
    return (qntChar, qntTotalPalavra, tuplas, palavra)


getReg = do
    n <- G.getWord8
    g <- G.getWord32be
    return (n, g)


getNum = do
    c <- G.getWord32be
    return c

getTex = do
    t <- G.getWord8
    return t

getTexts n = do
    if n == 0 then
        return []
    else
        do{
            t <- getTex;
            ts <- getTexts (n - 1);
            return (t:ts)
    }


getRegs v = do
    if v == 0 then do {
        return []
        }
        else do
            r <- getReg
            rs<- getRegs (v-1)
            return(r:rs) 

leitura = do
    putStr "Digite o nome do arquivo para ler\t"
    name_Arq <- getLine
    bys <- L.readFile name_Arq
    let (a,b,c,d) = G.runGet getTudo bys
    printRegs c
    putStrLn (show (map I.w2c d))


printRegs [] = return ()
printRegs (r:rs) = do
    printReg r
    printRegs rs


printReg (c, f) = putStrLn( (show (I.w2c c)) ++ "-" ++ show f)
