import Data.List
import Data.Char

type Doc = String
type Line = String
type Word = String

excluiCE [] = []
excluiCE ((x, xs):ys) = (x,eNormal xs):excluiCE ys
	where
		eNormal [] = []
		eNormal (z:zs) = if isAlpha z then z:(eNormal zs) else eNormal zs


enumLin xs = zip [1..]  (lines xs)

coisa a xs = (a, xs)

enumWords [] = []
enumWords ((x, xs):ys) = map (coisa x) (words xs) ++ enumWords ys
	where 

comparador (_,xs) (_,ys) = if (xs > ys) then GT else if (xs < ys) then LT else EQ

sortLs xs = sortBy comparador xs 

almalgamate ((x, xs):ys)  = x

co p [] = []
co p ((x, xs):ys) = if ( p == xs) then x:co p ys else []

alma [] = []
alma ((x, y):ys) = ( x:co y ys, y):alma ys

keep p [] = []
keep p ((x, xs):ys) = if ( p /= xs ) then (x, xs):ys else keep p ys

shorten [] = []
shorten ((x, y):ys) = (x,y):(shorten (keep y ys)) -- :shorten (keep y ys)

tudo xs = excluiCE (shorten (alma (sortLs (enumWords (enumLin (xs))))))

{- ====== <[  EX 1  ]> ======-}
tama [] = 0
tama (x:xs) = 1 + tama xs

ex1 xs = tama (words xs)

{- ====== <[  EX 2  ]> ======-}
meuLower [] = []
meuLower (x:xs) = toLower(x):meuLower xs

veQuaisTem _ [] = []
veQuaisTem st (x:xs) = if (isInfixOf (meuLower st) (meuLower x)) then x:(veQuaisTem st xs) else veQuaisTem st xs

ex2 st txt = veQuaisTem st (lines txt)
{-
main = do   putStr "Digite seu arquivo parca:\n"
            arq <- getLine
            txt <- readFile arq
            putStr "Qual string se quer vey?"
            str1 <- getLine
           -- putStr "Qual o nome do arquivo destino?\n"
           -- ardD <- getLine
            let o = ex2 str1 txt
            putStr (show o)
           -- writeFile ardD (fazBunitinho (show res))
            putStr "\n" -}

{- ====== <[  EX 3  ]> ======-}
trocaPal _ _ [] = []
trocaPal st1 st2 (x:xs) = if (st1 == x) then st2:trocaPal st1 st2 xs else x:(trocaPal st1 st2 xs)

fazCerto st1 st2 xs = trocaPal st1 st2 xsl

ex3 st1 st2 txt = fazCerto st1 st2 (lines txt)

main = do   putStr "Digite seu arquivo parca:\n"
            arq <- getLine
            txt <- readFile arq
            putStr "Qual string se quer vey?\n"
            str1 <- getLine
            putStr "Qual a nova string mano?\n"
            str2 <- getLine
            putStr "Qual o nome do novo arquivo cara?\n"
            novoA <- getLine

            let o = ex3 str1 str2 txt
            putStr (show o)

            writeFile novoA (unlines o)
            putStr "\n"
