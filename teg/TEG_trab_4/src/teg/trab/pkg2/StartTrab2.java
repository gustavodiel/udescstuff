/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teg.trab.pkg2;

import java.util.Scanner;


/**
 *
 * @author gustavo
 */
public class StartTrab2 {

    static void start(String[] args) {
        System.out.println("Digite qual das opcoes voce deseja:\n\t1. Mostrar Matriz de Adjacencia\n\t2. Mostrar Matriz de Incidencia\n\t3. Mostrar os graus dos nos\n");
        Scanner scn = new Scanner(System.in);
        int res = scn.nextInt();
        switch(res){
            case 1:
                TEGTrab4.grafo.printAdjacencia();
                break;
            case 2:
                TEGTrab4.grafo.printIncidencia();
                break;
            case 3:
                TEGTrab4.grafo.printGraus();
                break;
            default:
                System.out.println("Opcao invalida");
                break;
        }
        
        
    }

}
