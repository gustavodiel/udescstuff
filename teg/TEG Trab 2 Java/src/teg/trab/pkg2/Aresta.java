/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teg.trab.pkg2;

/**
 *
 * @author UDESC
 */
public class Aresta {
    int valor;
    String nome;
    boolean isDirected;
    Tupla<No> tupla;
    
    Aresta(No no1, No no2, boolean isDirected, int val, String no){
        tupla = new Tupla<>(no1, no2);
        this.isDirected = isDirected;
        valor = val;
        nome = no;
        
    }

    boolean connects(No a, No b) {
        return (tupla.no1 == a && tupla.no2 == b) || (tupla.no1 == b && tupla.no2 == a);
    }
    
    boolean connects(No a){
        return (tupla.no1 == a || tupla.no2 == a);
    }

    short getConnectionType(No no) {
        if (!isDirected){
            if (connects(no)){
                return 1;
            }
            return 0;
        }
        if (tupla.no1 == no){
            return 1;
        } else if (tupla.no2 == no){
            return -1;
        }
        return 0;
    }
    
    @Override
    public String toString(){
        return nome + "(" + valor + ")";
    }
}
