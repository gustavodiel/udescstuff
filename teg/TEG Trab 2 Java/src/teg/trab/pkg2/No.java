/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teg.trab.pkg2;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author UDESC
 */
public class No {
    HashMap<No, ArrayList<Aresta>> conexoes = new HashMap();
    String nome;
    
    No(){
        
    }
    
    No(String nome){
       this.nome = nome; 
    }
    
    Aresta addConecao(No adj, boolean isDirected){
        String nome;
        if (isDirected){
            nome = this + "->" + adj;
        } else {
            nome = this + "<->" + adj;
        }
        Aresta a = new Aresta(this, adj, isDirected, 0, nome );
        if (!conexoes.containsKey(adj)){
            conexoes.put(adj, new ArrayList());
        }
        conexoes.get(adj).add(a);
        if (!isDirected){
            adj.addConecao(this, a, isDirected);
        }
        return a;
    }
    
    Aresta addConecao(No adj, Aresta a, boolean isDirected){
        if (!conexoes.containsKey(adj)){
            conexoes.put(adj, new ArrayList());
        }
        conexoes.get(adj).add(a);
        return a;
    }
    
    int numConnections(No b){
        if (!conexoes.containsKey(b)){
            return 0;
        }
        return conexoes.get(b).size();
    }
    
    @Override
    public String toString(){
        return nome;
    }
}
