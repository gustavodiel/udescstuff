/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teg.trab.pkg2;

/**
 *
 * @author gustavo
 */
class StartTrab3 {

    static void start(String[] args) {
        int v[][] = TEGTrab2.grafo.generateAdjMatrix();
        System.out.println("Matriz de incidência:");
        for (int i = 0; i < v.length; i++) {
            System.out.print(String.format("%-3s", i));
            for (int j = 0; j < v.length; j++) {
                System.out.print(String.format("%3s", v[i][j]));
            }
            System.out.println("");

        }
    }

}
