/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teg.trab.pkg2;

import java.util.ArrayList;

/**
 *
 * @author gustavo
 */
public class Graph {

    ArrayList<Aresta> arestas;
    ArrayList<No> vertices;

    Graph() {
        arestas = new ArrayList();
        vertices = new ArrayList();
    }

    No addNo() {
        String nome = String.valueOf(vertices.size());
        No no = new No(nome);
        vertices.add(no);
        return no;
    }

    No addNo(String nome) {
        No no = new No(nome);
        vertices.add(no);
        return no;
    }

    No addNo(No no) {
        vertices.add(no);
        return no;
    }

    void addConection(String a, String b, boolean dir) {
        No na = null, nb = null;
        for (No vertice : vertices) {
            if (vertice.nome.equals(a)) {
                na = vertice;
            }
            if (vertice.nome.equals(b)) {
                nb = vertice;
            }
        }
        if (na == null) {
            na = addNo(a);
        }
        if (nb == null) {
            nb = addNo(b);
        }

        addConection(na, nb, dir);
    }

    void addConection(No a, String b, boolean dir) {
        No nb = null;
        for (No vertice : vertices) {
            if (vertice.nome.equals(b)) {
                nb = vertice;
                break;
            }
        }
        if (nb == null) {
            nb = new No(b);
        }

        addConection(a, nb, dir);
    }

    void addConection(No a, No b, boolean dir) {
        arestas.add(a.addConecao(b, dir));
    }

    No getNo(int i) {
        return vertices.get(i);
    }

    Aresta getAresta(int i) {
        return arestas.get(i);
    }

    ArrayList<Aresta> getPair(No a, No b) {
        ArrayList<Aresta> res = new ArrayList();
        arestas.stream().filter((aresta) -> (aresta.connects(a, b))).forEachOrdered((aresta) -> {
            res.add(aresta);
        });
        return res;
    }

    void printAdjacencia() {
        System.out.println("Matriz de adjacência:");
        int totalVertices = vertices.size();
        System.out.print("     ");
        for (int i = 0; i < totalVertices; i++) {
            System.out.print(String.format("%-3s", vertices.get(i).nome));
        }
        System.out.println("");
        No no;
        for (int i = 0; i < totalVertices; i++) {
            no = vertices.get(i);
            System.out.print(String.format("%-3s", no.nome));
            for (int j = 0; j < totalVertices; j++) {
                System.out.print(String.format("%3s", no.numConnections(vertices.get(j))));
            }
            System.out.println("");
        }
    }

    void printIncidencia() {
        System.out.println("Matriz de incidência:");
        short totalArestas = (short) arestas.size();
        System.out.print("   ");
        for (int i = 0; i < totalArestas; i++) {
            System.out.print(String.format("%3d", i));
        }
        System.out.println("");
        No no;
        for (int i = 0; i < vertices.size(); i++) {
            no = vertices.get(i);
            System.out.print(String.format("%-3s", no.nome));
            for (Aresta aresta : arestas) {
                short tp = aresta.getConnectionType(no);
                System.out.print(String.format("%3s", tp));
            }
            System.out.println("");
        }
    }

    int[][] generateAdjMatrix() {
        int v = vertices.size();
        int mat[][] = new int[v][v];
        for (int i = 0; i < v; i++) {
            No no = vertices.get(i);
            for (int j = 0; j < v; j++) {
                mat[i][j] = no.numConnections(vertices.get(j));
            }
        }

        return mat;
    }

}
