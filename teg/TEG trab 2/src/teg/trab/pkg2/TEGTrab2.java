/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teg.trab.pkg2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Scanner;


/**
 *
 * @author gustavo
 */
public class TEGTrab2 {
    
    static Graph grafo = new Graph();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner scn = new Scanner(System.in);
        System.out.println("Bom dia usuario! Este grafo e direcionado? ( s / n )");
        
        String fileName;
        if (args.length > 1) {
            fileName = args[1];
        } else {
            fileName = "trab2.txt";
        }

        //read file into stream, try-with-resources
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String curNo = null;
            String line;
            while ((line = br.readLine()) != null) {
                String[] res = line.split(" ");
                for (String re : res) {
                    if (re.contains(":")) {
                        curNo = re.substring(0, re.length() - 1);
                    } else {
                        grafo.addConection(curNo, re, true);
                    }
                }
            }

        } catch (IOException e) {
            System.out.print("Cant open file in :");
            Path currentRelativePath = Paths.get("");
            String s = currentRelativePath.toAbsolutePath().toString();
            System.out.println(s);
        }

        Collections.sort(grafo.vertices, (No fruit2, No fruit1) -> Integer.valueOf(fruit2.nome).compareTo(Integer.valueOf(fruit1.nome)));
        
        // TODO code application logic here
        System.out.println("Escolha a opcao desejada:");
        System.out.println("2. Matriz de Adjacencia e Incidencia ( por listas )");
        
        int res = scn.nextInt();
        switch(res){
            case 2:
                StartTrab2.start(null);
                break;
            default:
                System.out.println("Usuario, esta opcao e invalida!");
                break;
        }
    }

}
