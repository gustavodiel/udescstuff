#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG

#define NUM_NOS_POR_ALLOC 20
#define TAM_MAX_LINHA 256

typedef struct
{
    int tamanho;
    int indiceOrigem;
    int indiceDestino;
    int biDirecional;
} aresta;

typedef struct
{
    int numVizinhos;
    int* vizinhos;
    aresta** arestas;
} *no;



void matrixAdjInci(int, const char* args[]);

int main(int argc, const char* args[])
{
    printf("Hello world!\nBem vindo ao nosso compilado de trabalhos de TEG\n\t\tTeoria dos Grafos\n");
MENU:
    printf("Por favor, selecione uma tarefa abaixo:\n");

    printf("\t1\t- Implementar matrizes de adjacencia e de incidencia\n");
    printf("\t0\t- Sair\n");

    printf("\t");

    int op;
    scanf("%d", &op);
    switch (op)
    {
        case 1:
            matrixAdjInci(argc, args);
            goto MENU;
        case 0:
            printf("Goodbye World..\n");
            break;
        default:
            printf("Opcao invalida senhor(a)!\n");
            goto MENU;
    }

    return 0;
}

// Fun�ao que ir� criar e representar as matrizes de adjacencia e incidencia
void matrixAdjInci(int argc, const char* args[])
{
    printf("%d\n", argc);
    // ISSO � APENAS PARA TESTAR O PROGRAMA
#ifndef DEBUG
    if (argc < 2)
    {
        printf("Por favor execute o programa com o arquivo. Ex: /main arquivoEntrada.txt\n");
        return;
    }
    FILE* arq = fopen(args[1], "r");
#else
    FILE* arq = fopen("trab1_input.txt", "r");
#endif // DEBUG

    if (!arq)
    {
        printf("Falha ao abrir o arquivo!\n");
        return;
    }
    no* vetor_nos = malloc(sizeof(no) * NUM_NOS_POR_ALLOC);
    aresta** todasArestas = malloc(sizeof(aresta*) * NUM_NOS_POR_ALLOC);

    int i;
    for(i = 0; i < NUM_NOS_POR_ALLOC; ++i)
    {
        vetor_nos[i] = malloc(sizeof(no));
        vetor_nos[i]->numVizinhos = 0;
        vetor_nos[i]->vizinhos = malloc(sizeof(int) * NUM_NOS_POR_ALLOC);
        vetor_nos[i]->arestas = malloc(sizeof(aresta*) * NUM_NOS_POR_ALLOC);

        todasArestas[i] = malloc(sizeof(aresta));
        todasArestas[i]->tamanho = 1;
    }


    char buffer[TAM_MAX_LINHA];
    char c;
    int charAtual       = 0,
    noAtual             = 0,
    qntNos              = 0,
    qntArresta          = 0,
    arrestaAtual        = 0,
    realocs             = 1,
    realocsVizinhos     = 1,
    realocsArrestas     = 1,
    terminouBarraN      = 0,
    proxEspacoIgnore    = 0;

    while ((c = fgetc(arq)) != EOF)
    {
        if( c == ':')
        {
            proxEspacoIgnore = 1;
            continue;
        }
        if (proxEspacoIgnore)
        {
            proxEspacoIgnore = 0;
            charAtual = 0;
            memset(buffer, 0, TAM_MAX_LINHA);
            continue;
        }

        if (c == '\n' || c==' '){
            if (vetor_nos[noAtual]->numVizinhos >= realocsVizinhos * NUM_NOS_POR_ALLOC)
            {
                ++realocsVizinhos;
                vetor_nos[noAtual]->vizinhos = realloc(vetor_nos[noAtual]->vizinhos, realocsVizinhos * NUM_NOS_POR_ALLOC);
                for (i = 0; i < NUM_NOS_POR_ALLOC; i++)
                {
                    int num = i + (realocsVizinhos - 1)*NUM_NOS_POR_ALLOC;
                    vetor_nos[noAtual]->vizinhos[ num ] = malloc(sizeof(int));
                    
                }
            }
            vetor_nos[noAtual]->vizinhos[vetor_nos[noAtual]->numVizinhos] = atoi(buffer);
            vetor_nos[noAtual]->numVizinhos++;
            
            if (qntArresta >= realocsArrestas * NUM_NOS_POR_ALLOC){
                ++realocsArrestas;
                todasArestas = realloc(todasArestas, realocsArrestas * NUM_NOS_POR_ALLOC);
                for (i = 0; i < NUM_NOS_POR_ALLOC; i++)
                {
                    int num = i + (realocsArrestas - 1)*NUM_NOS_POR_ALLOC;
                    todasArestas[ num ] = malloc(sizeof(aresta));
                    
                }
            }
            
            todasArestas[arrestaAtual]->tamanho = 1;
            todasArestas[arrestaAtual]->indiceOrigem = noAtual;
            todasArestas[arrestaAtual]->indiceDestino = atoi(buffer);
            arrestaAtual++;
            qntArresta++;
            // Para saber se � bidirecional, temos que
                
            charAtual = 0;
            if (c == '\n')
            {
                noAtual++;
                qntNos++;
                realocsVizinhos = 1;
                if (qntNos >= realocs * NUM_NOS_POR_ALLOC)
                {
                    realocs++;
                    vetor_nos = realloc(vetor_nos, sizeof(no) * realocs * NUM_NOS_POR_ALLOC );
                    for (i = 0; i < NUM_NOS_POR_ALLOC; i++)
                    {
                        int num = i + (realocs - 1)*NUM_NOS_POR_ALLOC;
                        vetor_nos[ num ] = malloc(sizeof(no));
                        vetor_nos[ num ]->numVizinhos = 0;
                        vetor_nos[ num ]->vizinhos = malloc(sizeof(int) * NUM_NOS_POR_ALLOC);
                        vetor_nos[ num ]->arestas = malloc(sizeof(aresta*) * NUM_NOS_POR_ALLOC);
                    }
                }
                terminouBarraN = 1;
                memset(buffer, 0, TAM_MAX_LINHA);
            }
            else if (c == ' ')
            {

                terminouBarraN = 0;
                memset(buffer, 0, TAM_MAX_LINHA);
            }
        }
        else if (c != ':')
        {
            terminouBarraN = 0;
            buffer[charAtual++] = c;
        }

    }
    // Temos que realizar a operacao para o ultimo no, ja que nao chegara em um \n
    if (!terminouBarraN)
    {
        if (vetor_nos[noAtual]->numVizinhos >= realocsVizinhos * NUM_NOS_POR_ALLOC)
        {
            ++realocsVizinhos;
            vetor_nos[noAtual]->vizinhos = realloc(vetor_nos[noAtual]->vizinhos, realocsVizinhos * NUM_NOS_POR_ALLOC);
        }
        vetor_nos[noAtual]->vizinhos[vetor_nos[noAtual]->numVizinhos] = atoi(buffer);
        vetor_nos[noAtual]->numVizinhos++;
        noAtual++;
        qntNos++;
        realocsVizinhos = 1;
        charAtual = 0;
        if (qntNos >= realocs * NUM_NOS_POR_ALLOC)
        {
            realocs++;
            vetor_nos = realloc(vetor_nos, sizeof(no) * realocs * NUM_NOS_POR_ALLOC );
            for (i = 0; i < NUM_NOS_POR_ALLOC; i++)
            {
                int num = i + (realocs - 1)*NUM_NOS_POR_ALLOC;
                vetor_nos[ num ] = malloc(sizeof(no));
                vetor_nos[ num ]->numVizinhos = 0;
                vetor_nos[ num ]->vizinhos = malloc(sizeof(int) * NUM_NOS_POR_ALLOC);
                vetor_nos[ num ]->arestas = malloc(sizeof(aresta*) * NUM_NOS_POR_ALLOC);
            }
        }
        memset(buffer, 0, TAM_MAX_LINHA);
    }
    int k;
    int j;
    printf("Matriz de Adjacencia:\n\n");
    for (i = 0; i < qntNos; i++)
    {
        printf("\t%d", i);
    }
    printf( "\n");
    for (i = 0; i < qntNos; i++)
    {
        printf("%d:\t", i);
        for ( j = 0; j < qntNos; j++)
        {
            int foi = 0;
            for ( k = 0; k < vetor_nos[i]->numVizinhos; k++)
            {
                // printf("%d %d\n", j, vetor_nos[i]->vizinhos[k]);
                if (j == vetor_nos[i]->vizinhos[k])
                {

                    printf("1\t");
                    foi = 1;
                    break;
                }
            }
            if (!foi)
            {
                printf("0\t");
            }
        }

        printf("\n");
    }
    printf("Matriz de Incidencia:\n\n");
    for (i = 0; i < qntArresta; i++)
    {
        printf("%-5d", i);
    }
    printf( "\n");
    for (i = 0; i < qntNos; i++)
    {
        printf("%d:\t", i);
        for ( j = 0; j < qntNos; j++)
        {
            int foi = 0;
            for ( k = 0; k < vetor_nos[i]->numVizinhos; k++)
            {
                // printf("%d %d\n", j, vetor_nos[i]->vizinhos[k]);
                if (j == vetor_nos[i]->vizinhos[k])
                {

                    printf("1\t");
                    foi = 1;
                    break;
                }
            }
            if (!foi)
            {
                printf("0\t");
            }
        }

        printf("\n");
    }
}
