#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define DEBUG

#define NUM_POR_ALLOC 5
#define TAM_MAX_LINHA 256


void matrixAdjInci(int, const char* args[]);

int main(int argc, const char* args[])
{
    printf("Hello world!\nBem vindo ao nosso compilado de trabalhos de TEG\n\t\tTeoria dos Grafos\n");
MENU:
    printf("Por favor, selecione uma tarefa abaixo:\n");

    printf("\t1\t- Implementar matrizes de adjacencia e de incidencia\n");
    printf("\t0\t- Sair\n");

    printf("\t");

    int op;
    scanf("%d", &op);
    switch (op)
    {
        case 1:
            matrixAdjInci(argc, args);
            goto MENU;
        case 0:
            printf("Goodbye World..\n");
            break;
        default:
            printf("Opcao invalida senhor(a)!\n");
            goto MENU;
    }

    return 0;
}

// Funçao que irá criar e representar as matrizes de adjacencia e incidencia
void matrixAdjInci(int argc, const char* args[])
{
    printf("%d\n", argc);

    int** grafo = NULL;
    int** arestas = malloc(sizeof(int*) * NUM_POR_ALLOC);
    int* numArestas = malloc(sizeof(int) * NUM_POR_ALLOC);
    int* vertices = malloc(sizeof(int) * NUM_POR_ALLOC);

    memset(numArestas, 0, sizeof(int) * NUM_POR_ALLOC);
    memset(vertices, 0, sizeof(int) * NUM_POR_ALLOC);

    // ISSO � APENAS PARA TESTAR O PROGRAMA
#ifndef DEBUG
    if (argc < 2)
    {
        printf("Por favor execute o programa com o arquivo. Ex: /main arquivoEntrada.txt\n");
        return;
    }
    FILE* arq = fopen(args[1], "r");
    printf("Arquivo aberto: %s\n", args[1]);
#else
    FILE* arq = fopen("trab1_input.txt", "r");
    printf("Arquivo aberto: %s\n", "trab1_input.txt");
#endif // DEBUG

    if (!arq)
    {
        printf("Falha ao abrir o arquivo!\n");
        return;
    }

    char buffer[TAM_MAX_LINHA];
    char c;

    int charAtual = 0, arestaAtual, verticeAtual, totalArestas = 0, totalVertice = 0, breakNext = 0;
    int allocsGrafo = 1, allocsArestas = 1;

    int maiorVertice = 0;


    while (1)
    {
        c = fgetc(arq);

        if (breakNext){
            breakNext = 0;
            continue;
        }
        if (c == '\n' || c==' ' || c == EOF){
            if(buffer[0] != '\0'){
                arestas[ vertices[ totalVertice ] ][ numArestas[ vertices[ totalVertice ] ] ] = atoi(buffer);
                printf("%d\n", arestas[ vertices[ totalVertice ] ][ numArestas[ vertices[ totalVertice ] ] ]);
                numArestas[ totalVertice ]++;

                memset(buffer, 0, TAM_MAX_LINHA);

                charAtual = 0;
            }
            if (c == '\n')
            {
                totalVertice++;
            } else if (c == ' ')
            {

            } else
            {
                break;
            }
        }
        else if (c == ':')
        {
            verticeAtual = atoi(buffer);
            maiorVertice = verticeAtual > maiorVertice ? verticeAtual : maiorVertice;
            vertices[totalVertice] = verticeAtual;

            if (vertices[ totalVertice ] > allocsArestas * NUM_POR_ALLOC){
                allocsArestas++;
                arestas = realloc(arestas, sizeof(int*) * NUM_POR_ALLOC * allocsArestas);
            }
            printf("Allocado para %d\n", totalVertice);
            arestas[totalVertice] = malloc(sizeof(int) * NUM_POR_ALLOC);

            memset(buffer, 0, TAM_MAX_LINHA);
            charAtual = 0;
            breakNext = 1;
        } else
        {
            buffer[charAtual++] = c;
        }

    }
    printf("Numero de vertices: %d, maior: %d\n", totalVertice, maiorVertice);
    printf("Matriz de Adjacencia:\n");

    grafo = malloc(sizeof(int*) * maiorVertice);

    int i;
    for (size_t i = 0; i < totalVertice; i++) {
        grafo[vertices[i]] = malloc(sizeof(int*) * totalVertice);
    }

    for (size_t i = 0; i < totalVertice; i++) {
        for (size_t j = 0; j < totalVertice; j++) {
            int foi = 0;
            for (size_t k = 0; k < numArestas[ i ]; k++) {
                if (arestas[vertices[i]][k] == j)
                {
                    foi = 1;
                    break;
                }
            }
            grafo[vertices[i]][j] = foi;
        }
    }
    printf("Vertices\t\t");
    for (size_t i = 0; i < totalVertice; i++) {
        printf("%d\t", vertices[i]);
    }
    printf("\n");
    for (size_t i = 0; i < totalVertice; i++) {
        printf("\t\t%d:\t", vertices[i]);
        for (size_t j = 0; j < totalVertice; j++) {
            printf("%d\t", grafo[vertices[i]][j]);
        }
        printf("\n");
    }



    close(arq);
    free(vertices);
    free(numArestas);
    for (size_t i = 0; i < NUM_POR_ALLOC * allocsGrafo; i++) {
        free(grafo[i]);
    }
    free(grafo);

    for (size_t i = 0; i < NUM_POR_ALLOC * allocsArestas; i++) {
        free(arestas[i]);
    }
    free(arestas);

}
