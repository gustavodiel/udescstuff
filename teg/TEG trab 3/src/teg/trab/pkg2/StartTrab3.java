/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teg.trab.pkg2;

/**
 *
 * @author gustavo
 */
class StartTrab3 {

    static void start(String[] args) {
        int v[][] = TEGTrab3.grafo.generateAdjMatrix();
        StartTrab3 t = new StartTrab3();
        t.seConexo(TEGTrab3.grafo);
    }

    void seConexo(Graph grafo) {
        int adj[][] = grafo.generateAdjMatrix();
        int num_vertices = grafo.vertices.size();
        int visitado[][] = new int[num_vertices][num_vertices];
        int vertVisit[] = new int[num_vertices];
        int mud, linha=0;
        int i, j;
        int num_subgrafo = 1, subAtual;

        vertVisit[linha] = num_subgrafo;
        j = 0;
        i = linha;
        do {
            if (vertVisit[linha] == 0) {
                num_subgrafo++;
                vertVisit[linha] = num_subgrafo;
            }
            if (vertVisit[linha] > 0) {
                do {
                    mud = 0;
                    do {
                        if ((adj[i][j] > 0) && (visitado[i][j] == 0) && (i != j)) {
                            int aux = i;
                            vertVisit[j] = vertVisit[i];
                            visitado[i][j] = 1;
                            i = j;
                            j = aux;
                            visitado[i][j] = 1;
                            mud = 1;
                        } else {
                            visitado[i][j] = 1;
                        }
                        j++;
                    } while (j < num_vertices);
                    j = 0;
                    i = linha;
                } while (mud == 1);
            }
            linha++;
            i = linha;
            j = 0;
        } while (linha < num_vertices);

        if (num_subgrafo == 1) {
            System.out.println("\nGrafo Conexo\n");
        } else {
            System.out.println("\nGrafo desconexo! Possui " + num_subgrafo + " subgrafos\n");
        }
    }

}
