/*
 * =====================================================================================
 *
 *       Filename:  trabalho_1.h
 *
 *    Description:  Header para o trbalho 1
 *
 *        Version:  1.0
 *        Created:  03/10/17 08:18:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Gustavo Diel (GD), gustavodiel@hotmail.com
 *        Company:  UDESC
 *
 * =====================================================================================
 */

#ifndef TRABALHO_1_H
#define TRBALHO_1_H

//---------------------------------ADJACENCIA----------------------------------------------------------------------

int** adicionaVertice(int qtdVert, int **p);
int** geraMatriz(int qtdVert);
void mostraGrau(int grau, int qtdVert, int **p);
void imprimeMatriz(int qtdVert, int **p);
void grafoComplementar(int qtdVert, int **p);
void zeraMatriz(int qtdVert, int **p);
int relacaoes(int **p);
void removeVertice(int qtdVert, int vertDel, int **p);

//---------------------------------INCIDENCIA----------------------------------------------------------------------

void mostraGrauIncidencia(int grau, int qtdAres, int **p);
int relacaoIncidencia(int **p);
int relacaoIncidenciaDirecionada(int **p);
int **geraMatrizIncidencia(int qtdVert,int qtdAres);
void zeraMatrizIncidencia (int qtdAres, int qtdVert, int **p);
void imprimeMatrizIncidencia (int qtdAres,int qtdVert, int **p);
void removeVerticeIncidencia(int qtdAres, int vertDel, int **p);
int** adicionaVerticeIncidencia(int qtdVert, int **p);

#endif

