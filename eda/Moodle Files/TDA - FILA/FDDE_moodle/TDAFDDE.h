
/* no turboC para DOS use:
 #include "client~1.h"*/


#define LIVRE 0
#define OCUPADA 1
#define FRACASSO 0
#define SUCESSO 1
#define FALSO 0
#define VERDADEIRO 1
#define NAO 0
#define SIM 1
#define MAIOR 1
#define IGUAL 0
#define MENOR -1
#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#ifdef  __unix__
   #define clrscr() printf("[2J")
#else
/*  #include <stdlib.h>*/
   #define clrscr() system("cls")
#endif

typedef struct Fila *pFila, **ppFila;




int cria(ppFila pp, int tamInfo);
int insere(pFila p, void * novo);
int buscaNaFrente(pFila p, void *pReg);
int buscaNaCauda(pFila p, void *pReg);
int retira(pFila p, void *pReg);
void destroi(ppFila pp);
void purga(pFila p);
int testaVazia(pFila p);

void vaivem(pFila p, void (*  cmp)(void* x) );
