/*#include "tda_ve~1.h"*/


#ifdef debugging
    int aux;
#endif
#include "tdaFDDE_priv.h"


int cria(ppFila pp, int tamInfo)
{
  if ((*pp = (pFila) malloc(sizeof(Fila))) == NULL)
    return FRACASSO;
  else
   { (*pp)->frente = (*pp)->cauda = NULL;
     (*pp)->tamInfo = tamInfo;
      return SUCESSO;
   }
}


int testaVazia(pFila p)
{ if(p->frente == NULL && p->cauda==NULL)
    return SIM;
  else
     return NAO;
}

int insere(pFila p, void *novo)
{ int result;
  pNoFila temp, temp2;
  if ((temp = (NoFila *) malloc(sizeof(NoFila))) == NULL)
        		return FRACASSO;
  else
     if ((temp->dados = (void *)malloc(p->tamInfo))== NULL)
      { free(temp);
        return FRACASSO;
      }
      else
       { memcpy(temp->dados,novo, p->tamInfo);

  		 if(p->frente == NULL) /*vazia*/
    		{   temp->atras = temp->defronte = NULL;
    		    p->frente = p->cauda = temp;
    		}
    		else
    		{  p->cauda->atras=temp;
    		   temp->defronte=p->cauda;
    		   p->cauda=temp;
    		   temp->atras=NULL;
               return SUCESSO;
    		}
        }
     return SUCESSO;

}

int buscaNaFrente(pFila p, void *pReg)
{ if (p->frente == NULL)
  		return FRACASSO;
  else
     { memcpy(pReg,p->frente->dados, p->tamInfo);
     return SUCESSO;
     }
}

int buscaNaCauda(pFila p, void *pReg)
{ if (p->frente == NULL)
  		return FRACASSO;
  else
     {  memcpy(pReg,p->cauda->dados, p->tamInfo);
     return SUCESSO;
     }
}

int retira(pFila p, void *pReg)
{
  if (p->frente == NULL)
  		return FRACASSO;
  else
     { memcpy(pReg,p->frente->dados, p->tamInfo);
       if(p->frente == p->cauda)
        {   free(p->frente->dados);
            free(p->frente);
            p->frente = p->cauda = NULL;
        }
       else
        {  	p->frente = p->frente->atras;
            free(p->frente->defronte->dados);
            free(p->frente->defronte);
            p->frente->defronte = NULL;
        }
       return SUCESSO;
     }
}

void purga(pFila p)
{
  while(p->frente->atras != NULL)
  {  p->frente = p->frente->atras;
     free(p->frente->defronte->dados);
     free(p->frente->defronte);
  }
  free(p->frente->dados);
  free(p->frente);
  p->frente = p->cauda = NULL;
}

void destroi(ppFila pp)
{ purga(*pp);
  free(*pp);
  *pp = NULL;
}

void vaivem(pFila p, void (*  cmp)(void* x) )
{ pNoFila aux=NULL;
  if (testaVazia==SIM)
    return;
  else
  { aux=p->frente;
    while(aux)
    {  cmp(aux->dados);
       aux=aux->atras;
    }
    aux=p->cauda;
    while(aux)
    { cmp(aux->dados);
      aux=aux->defronte;
    }
  }
}
