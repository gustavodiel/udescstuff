#ifdef debugging
    int aux;
#endif

#include "tdaFilaPriv.h"


int cria(ppFila pp, int tamInfo)
{
  if ((*pp = (pFila) malloc(sizeof(Fila))) == NULL)
    return FRACASSO;
  else
   { (*pp)->cauda=(*pp)->frente = NULL;
     (*pp)->tamInfo = tamInfo;
      return SUCESSO;
   }
}


int testaVazia(pFila p)
{ if(!p->cauda && !p->frente)
    return SIM;
  else
     return NAO;
}


int exibeCaudaUpTofrente(pFila p, void (* exibe)(void *p))
{ pNoFila aux=NULL;
  if (testaVazia( p) == SIM)
  		return FRACASSO;
  else
     { aux=p->cauda;
       while (aux)// (aux->defronte != NULL)
         { (* exibe)(aux->dados);
            aux = aux->defronte;
         }
      
         return SUCESSO;
     }

}


int insere(pFila p, void *info,int (*  cmp)(void* x, void* y))
{ int resp;
  pNoFila novo=NULL, visitado=NULL;
  if ((novo = (NoFila *) malloc(sizeof(NoFila))) == NULL)
        return FRACASSO;
  if ((novo->dados = (void *)malloc(p->tamInfo))== NULL)
      { free(novo);
        return FRACASSO; }
  else
   { memcpy(novo->dados,info, p->tamInfo);
     if (testaVazia( p) == SIM)
        { p->cauda=p->frente=novo;
          novo->atras=NULL;
          novo->defronte=NULL;
          return SUCESSO;
        }
     else
       {   /*maior prioridade na frente */
           /*comprara(novo,visitado): 
	   /*    novo > visitado retorna MAIOR (1)  */
	   /*    novo < visitado retorna MENOR (-1) */
	   /*    novo == visitado retorna MAIOR (0) */
	
	   visitado=p->frente;
            /* Se "novo" não é mais velho que o item na posição atual, então buscamos o elemento mais jovem do que ele
               e este vai estar mais para trás da posição atual, ou seja, em direção à entreda da fila. */
            {   
		while(visitado!= NULL && ((*cmp)(novo->dados, visitado->dados) != MAIOR)) 
                        visitado=visitado->atras;

                if(visitado == NULL) /* "novo" é o mais jovem de todos e tem que ser o item na cauda*/
         		{
         		    novo->defronte=p->cauda;
           		    novo->atras=NULL;
           		    p->cauda->atras=novo;
           		    p->cauda=novo;
	                    return SUCESSO;
         		}
                else /* "novo" encontrou um elemento Y, mais jovem. "visitado" aponta para este elemento Y, "novo"
                        será inserido à frente de Y.
                      */
                    {
                        novo->atras=visitado;
                        novo->defronte= visitado->defronte;
                        if(novo->defronte != NULL) 
	                    novo->defronte->atras=novo;	// "novo" item a frente de "visitado" e atrás de um outro item			
			else
                             p->frente = novo;// "novo" item é o de maior prioridade e entra pela "frente"
                        visitado->defronte=novo;
                        return SUCESSO;
                    }
            }

        }
    }
    return 1;
}

int buscaNaFrente(pFila p, void *pReg)
{ if (testaVazia( p) == SIM)
  		return FRACASSO;
  else
     { 
       memcpy(pReg,p->frente->dados, p->tamInfo);
       return SUCESSO;
     }
}

int buscaNaCauda(pFila p, void *pReg)
{ if (testaVazia( p) == SIM)
  		return FRACASSO;
  else
     { 
       memcpy(pReg,p->cauda->dados, p->tamInfo);
       return SUCESSO;
     }
}

int retira(pFila p, void *pReg)
{  pNoFila aux=NULL;
  if (testaVazia( p) == SIM)
  		return FRACASSO;
  else
     { 
       memcpy(pReg,p->frente->dados, p->tamInfo); 
       aux=p->frente->atras;
       free(p->frente);	 
       if(aux)
	{ p->frente=aux;
       	  p->frente->defronte=NULL;
         }
	else
	{ p->frente = p->cauda = NULL;
	}		 
       return SUCESSO;
     }
}

void purga(pFila p)
{ pNoFila aux;
  aux=p->cauda->atras;

  if (testaVazia( p) == NAO)
	{
	  while(p->cauda->defronte != NULL)
	  {  p->cauda=p->cauda->defronte;
	     free(p->cauda->atras->dados);
	     free(p->cauda->atras);
	  }
	  free(p->cauda->dados);
	  free(p->cauda);
	  
	  p->cauda=p->frente=NULL;
	}
}

void destroi(ppFila pp)
{ purga(*pp);
  free(*pp);
  *pp = NULL;
}


