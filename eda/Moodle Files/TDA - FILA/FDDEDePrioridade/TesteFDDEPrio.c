#include "ClienteFila.h"

void pause_(void);

int main(int argc, char *argv[])
{
  pFila ptr = NULL; 
 
  if(cria(&ptr, sizeof(info)) == FRACASSO)
     { puts("erro fatal durante a criação do tda");
       getchar();
     }
   else
	{ clrscr();
          menu(ptr);
	}
 return 1;
}

void menu(pFila p)
{  char opc;
   info auxInfo;

   /*int (* pFuncCompara )();*/

   do{ fflush(stdin);
   puts("FILA de PRIORIDADE");
   puts(" entre com uma opcao 1 (busca na frente), 2 (busca na cauda),");
   puts("                     3 (enfileira), 4 (remove da fila), 5 (purga), 6 (exibe a fila)");
   puts("                     0 (\"zero\") para encerrar");
   printf("\n");
	opc = getchar();
   clrscr();
     switch (opc)
	{ case '1':
               if(buscaNaFrente(p,&auxInfo)==FRACASSO)
			puts("fila vazia!");
               else
                	printf("\n consultado : %i \n", auxInfo.chave);
	       break;
     	  case '2':
            if(buscaNaCauda(p,&auxInfo)==FRACASSO)
               puts("fila vazia!");
            else
               printf("\n consultado : %i \n", auxInfo.chave);
	    break;
     	  case '3':
		printf("\nentre com a idade:\n");
            	fflush(stdin);
            	scanf("%i", &auxInfo.chave);
          // 	printf(" %i",compara(&auxInfo,&auxInfo));
           //	fflush(stdin);
           //	pause_();
            	if(insere(p,&auxInfo,compara)==FRACASSO)
             	 puts("erro na insercao: estrutura de dados cheia !");
              	break;
     	  case '4':
            if(retira(p, &auxInfo)==FRACASSO)
               puts("fila vazia!");
            else
               printf("\n item retirado : %i \n", auxInfo.chave);
            break;
     	  case '5' :
            purga(p);
            puts("limpou a fila !!!");
            break;
          case '6':
            exibeCaudaUpTofrente(p,exibe);
            break;
        }
     getchar();
   }while(opc != '0');
   puts("tchau");
   //system("sleep");

}

// Compara valores de idade
/*comprara(A,B): 
A > B retorna MAIOR (1)  
A < B retorna MENOR (-1) 
A == B retorna MAIOR (0) */
int compara(void *a, void *b)
{ if (((info*)a)->chave > ((info*)b)->chave)
     return MAIOR;
  else
     if (((info*)a)->chave < ((info*)b)->chave)
             return MENOR;
     else
        return IGUAL;
}

// Auxilia na exibição dos dados de um nó inserido
void exibe(void *a)
{ printf("%i \n",((info*)a)->chave);
  fflush(stdin);
  getchar();
}

/*void pause_(void)
{ char ch='\0';
  do
  { ch='\0';
    fflush(stdin);
    scanf("%c",&ch);
  }while (ch =='\0');

}*/
