#include "stdlib.h"
#include "stdio.h"
#include "string.h"
//#include "mem.h"

#define FRACASSO 0
#define SUCESSO 1
#define SIM 1
#define NAO 0


typedef struct FDSE *pFDSE, **ppFDSE;

int cria_f(ppFDSE pp, int tamInfo);

int insere_f(pFDSE p, void *novo);

int tamanho_f(pFDSE p);
void reinicia_f(pFDSE p);
void destroi_f(ppFDSE pp);

int buscaNoFim_f(pFDSE p, void *reg);
int buscaNoInicio_f(pFDSE p, void *reg);


int remove_f(pFDSE p, void *reg);

int testaVazia_f(pFDSE p);

int inverte_f(pFDSE p);
