//  gcc -c -Wall -O2 FDSE.c
//  ar rvs FDSE.a FDSE.o
// gcc -o aplica.exe cliente.c FDSE.a
#include "FDSE_priv.h"





/*************** CRIA ***************/
int cria_f(ppFDSE pp, int tamInfo)
{   	int ret = FRACASSO;
	if(((*pp)=(pFDSE)malloc(sizeof(FDSE)))==NULL)
		ret = FRACASSO;
	else
	{	(*pp)->cauda=NULL;
        (*pp)->frente=NULL;
        (*pp)->tamInfo=tamInfo;
        	ret = SUCESSO;
	}
    	return ret;
}


/*************** INSERE NA CAUDA ***************/
int insere_f(pFDSE p, void *novo)
{  	pNoFDSE temp;
   	int ret = FRACASSO;
	if((temp=(pNoFDSE)malloc(sizeof(NoFDSE)))!= NULL)
	{ if((temp->dados = (void *) malloc(p->tamInfo))!=NULL)
      {   memcpy(temp->dados,novo,p->tamInfo);
        	 	temp->atras = NULL;
		 	if(p->cauda==NULL && p->frente==NULL)
			 {	p->cauda=p->frente=temp;

		 	 }
		 	else
		 	{	p->cauda->atras=temp;
                p->cauda=temp;
		 	}
        	ret = SUCESSO;
       }
       else
      	free(temp);
   	}
   	return ret;
}


/*************** REMOVE DA FRENTE ***************/
int remove_f(pFDSE p, void *reg)
{  	int ret = FRACASSO;
    pNoFDSE aux=NULL;
	if(p->cauda!=NULL && p->frente!=NULL)
	{  	aux=p->frente->atras;
        memcpy(reg,p->frente->dados,p->tamInfo);
        free(p->frente->dados);
        free(p->frente);
        p->frente=aux;
        if(aux==NULL) // existia apenas um elemento inserido?
      	{ p->cauda=aux;
      	}
      	ret = SUCESSO;
	}
	return ret;
}



/*************** BUSCA NA FRENTE ***************/
int buscaNoInicio_f(pFDSE p, void *reg)
{  int ret = FRACASSO;
   if(p->frente != NULL)
	{ 	memcpy(reg,p->frente->dados,p->tamInfo);
      		ret = SUCESSO;
	}
   return ret;
}

/*************** VAZIA? ***************/
int testaVazia_f(pFDSE p)
{
   if(p->frente == NULL && p->cauda==NULL)
		return SIM;
   else
	return NAO;
}

/*************** BUSCA NA CAUDA ***************/
int buscaNoFim_f(pFDSE p, void *reg)
{	pNoFDSE aux;
	int ret = FRACASSO;
	if(p->cauda != NULL && p->frente !=NULL)
	{ aux=p->frente;
	  while(aux->atras!=NULL)
      		  aux=aux->atras;
	  memcpy(reg,aux->dados,p->tamInfo);
      	  ret = SUCESSO;
	}
  	return ret;
}


/*************** TAMANHO ***************/
int tamanho_f(pFDSE p)
{ int tam=0;
  pNoFDSE aux;
  aux=p->frente;
  if(aux==NULL)
    tam=0;
  else
  { while(aux!=NULL)
    { 	aux=aux->atras;
	tam++;
    }
  }
  return tam;
}

/*************** PURGA ***************/
void reinicia_f(pFDSE p)
{ pNoFDSE aux;

if(p->frente != NULL && p->cauda != NULL)
	{  aux=p->frente->atras;
	while(aux != NULL)
		{
         	free(p->frente->atras->dados);
		 	free(p->frente->atras);
		 	p->frente=aux;
		 	aux=aux->atras;
		}
	   	free(p->frente->dados);
	    free(p->frente);
      		p->frente = NULL;
      		p->cauda=NULL;
	}
}

/*************** DESTROI ***************/
void destroi_f(ppFDSE pp)
{
	pNoFDSE aux;

if((*pp)->frente != NULL && (*pp)->cauda != NULL)
	{  aux=(*pp)->frente->atras;
	while(aux != NULL)
		{
         	free((*pp)->frente->atras->dados);
		 	free((*pp)->frente->atras);
		 	(*pp)->frente=aux;
		 	aux=aux->atras;
		}
	   	free((*pp)->frente->dados);
	    free((*pp)->frente);
      		(*pp)->frente = NULL;
      		(*pp)->cauda=NULL;
	}
	free(*pp);
   	(*pp)=NULL;
}


