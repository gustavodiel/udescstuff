/*#include "tda_ve~1.h"*/
#include "tdaFESPriv.h"


int cria(ppFES pp, int capMax, int tamInfo)
{ int i, ret = SUCESSO;
   if (  ( *pp = (pFES) malloc(sizeof(tipoDescFES)) )!= NULL )             /* cria header */
   {  (*pp)->vet = (void **) malloc(capMax * sizeof(void *));     /* cria vetor */
       if ( (*pp)->vet !=NULL )
       { (*pp)->tamVet = capMax;             /*inicia tamanho do vetor*/
         (*pp)->tamInfo = tamInfo;           /*inicia pilha vazia */
         (*pp)->cauda = -1;                   /*pilha vazia, o cauda n�o indexa o vetor*/
         (*pp)->frente=0;
         for(i=0;i < capMax ;i++)            /*aloca mem�ria para inicializar os ponteiros void*/
	        if (((*pp)->vet[i] = (void*) malloc(tamInfo)) == NULL) /*ou *((*pp)->vet + i)  */
                { for(--i; i >=0;--i)        /* uma aloca��o falhou, deve-se desfazer o TDA */
                      free((*pp)->vet[i]);   /* ou  *((*pp)->vet + i)  */
                      free((*pp)->vet);   /*libera o vetor*/
		      free(*pp);    /*libera o descritor*/
		      *pp=NULL;  /*anula o ponteiro passado por referencia: pilha n�o criada */
                      i = capMax;                               /* para quebrar o la�o for mais externo */
                      ret = FRACASSO;
                 }
       }
       else
         {  free(*pp);    /*libera o descritor*/
            *pp = NULL;   /*anula o ponteiro passado por referencia: pilha n�o criada */
 	    ret = FRACASSO;
         }
    }
    else
        ret = FRACASSO;
  return ret;
}

void destroi(ppFES pp)
{ int i;
   for(i = 0; i< (*pp)->tamVet; i++)  /*libera mem�ria que inicializa os ponteiros void*/
     free( (*pp)->vet[i]);   /* equivalente a: *(p->vet + i));*/
  free((*pp)->vet);
  free(*pp);
  *pp = NULL;
}


/*-------------------------------------------------------------------------------*/
void purga(pFES p)
{
    p->cauda = -1;
    p->frente = 0;
}
/*-----------------------------------------------------------------------------*/
int buscaNocauda(pFES p, void * pReg) {
 int ret;
 if (p->cauda < p->frente) /* acesso direto ao cauda */
   ret = FRACASSO;
 else
 {
   memcpy(pReg, (p->vet[p->cauda]), p->tamInfo);// equivalente a: memcpy(pReg,*(p->vet+p->cauda), p->tamInfo);
   ret = SUCESSO;
 }
 return ret;
}
int buscaNofrente(pFES p, void * pReg) {
 int ret;
 if (p->cauda < p->frente) /* acesso direto ao cauda */
   ret = FRACASSO;
 else
 {
   memcpy(pReg, (p->vet[p->frente]), p->tamInfo);// equivalente a: memcpy(pReg,*(p->vet+p->cauda), p->tamInfo);
   ret = SUCESSO;
 }
 return ret;
}
/*------------ATEN��O: pelo conceito de transa��o at�mica
desempilha  #apenas# decrementaria o cauda ---------------------*/
int desenfileira(pFES p)
{ if (p->cauda < p->frente)
       return FRACASSO;
  else
  {  /* memcpy(pReg, *(p->vet+p->cauda) , p->tamInfo); */
    p->frente++;
      return SUCESSO;
   }
}

/*------------------------------------------------------------------------------*/
int enfileira(pFES p, void * novo)
{ if (p->cauda < p->tamVet-1)
   {
       p->cauda++;
        /*c�pia um bloco de tamanho definido deste o endere�o novo at� o endere�o
	definido por *(p->vet+p->cauda) */
       memcpy(*(p->vet+p->cauda), novo, p->tamInfo); /* equivalente a: memcpy(p->vet[p->cauda],novo,p->taminfo);*/
       return SUCESSO;
    }
    else
return FRACASSO;
}


/*--------------------------------------------------------------------------------*/
int testaVazia(pFES p) {
    int ret;
    if (p->cauda < p->frente) /* acesso direto ao cauda */
       ret = SIM;
    else
       ret = NAO;
    return ret;
}

/*---------------------------------------------------------------------------------*/
int testaCheia(pFES p) {
  int ret;
  if (p->cauda >= p->tamVet-1) /* acesso direto ao cauda */
    ret = SIM;
  else
    ret = NAO;
return ret;
}

