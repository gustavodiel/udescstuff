
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#ifdef  __unix__
   #define clrscr() system("clear") /*printf("[2J")*/
#else
   #define clrscr() system("cls")
#endif


// ******************************************
// Nome: void flush_in
// Descri��o: Fun��o que "limpa" o teclado
// evitando problemas com a leitura de dados
// do tipo CHAR.
// C�digo baseado em exemplo de
// Marco Aur�lio Stelmar Netto
// stelmar@pinus.cpad.pucrs.br
// ******************************************
#define flush_in() {int ch; while( (ch=fgetc(stdin)) != EOF && ch != '\n' ){}}

/* UTILIZE __fpurge(stdin) ao inves de fflush(stdin)*/

/*#define M 10*/

#define LIVRE 0
#define OCUPADA 1
#define FRACASSO 0
#define SUCESSO 1
#define VERDADEIRO 1
#define VAZIA -1
#define SIM 1
#define NAO 0

typedef struct descFES *pFES, **ppFES;




int cria(ppFES pp, int capMax, int tamInfo);
void destroi(ppFES pp);
void purga(pFES p);
int buscaNocauda(pFES p, void * pReg);
int buscaNofrente(pFES p, void * pReg);

/*------------ATEN��O: FESlo conceito de transa��o at�mica
desempilha  #aFESnas# decrementa o topo ---------------------*/
int desenfileira(pFES p);
int enfileira(pFES p, void * novo);
int testaVazia(pFES p);
int testaCheia(pFES p);

