
#include "stdio.h"
#include "stdlib.h"
#include "string.h" 

#ifdef  __unix__
   #define clrscr() system("clear") /*printf("[2J")*/
#else
   #define clrscr() system("cls")
#endif


// ******************************************
// Nome: void flush_in
// Descri��o: Fun��o que "limpa" o teclado
// evitando problemas com a leitura de dados
// do tipo CHAR.
// C�digo baseado em exemplo de
// Marco Aur�lio Stelmar Netto
// stelmar@pinus.cpad.pucrs.br
// ******************************************
#define flush_in() {int ch; while( (ch=fgetc(stdin)) != EOF && ch != '\n' ){}}

/* UTILIZE __fpurge(stdin) ao inves de fflush(stdin)*/ 

/*#define M 10*/

#define LIVRE 0
#define OCUPADA 1
#define FRACASSO 0
#define SUCESSO 1
#define VERDADEIRO 1
#define VAZIA -1
#define SIM 1
#define NAO 0

typedef struct descPE *pPE, **ppPE;

 


int cria(ppPE pp, int capMax, int tamInfo);
void destroi(ppPE pp);
void purga(pPE p);
int buscaNoTopo(pPE p, void * pReg);

/*------------ATEN��O: pelo conceito de transa��o at�mica
desempilha  #apenas# decrementa o topo ---------------------*/
int desempilha(pPE p);
int empilha(pPE p, void * novo);
int testaVazia(pPE p);
int testaCheia(pPE p);

