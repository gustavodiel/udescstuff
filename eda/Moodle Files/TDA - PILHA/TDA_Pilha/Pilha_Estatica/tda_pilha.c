/*#include "tda_ve~1.h"*/
#include "tdaPilhaPriv.h"


int cria(ppPE pp, int capMax, int tamInfo)
{ int i, ret = SUCESSO;
   if (  ( *pp = (pPE) malloc(sizeof(tipoDescPE)) )!= NULL )             /* cria header */
   {  (*pp)->vet = (void **) malloc(capMax * sizeof(void *));     /* cria vetor */
       if ( (*pp)->vet !=NULL )
       { (*pp)->tamVet = capMax;             /*inicia tamanho do vetor*/
         (*pp)->tamInfo = tamInfo;           /*inicia pilha vazia */
         (*pp)->topo = -1;                   /*pilha vazia, o topo n�o indexa o vetor*/
         for(i=0;i < capMax ;i++)            /*aloca mem�ria para inicializar os ponteiros void*/
	        if (((*pp)->vet[i] = (void*) malloc(tamInfo)) == NULL) /*ou *((*pp)->vet + i)  */
                { for(--i; i >=0;--i)        /* uma aloca��o falhou, deve-se desfazer o TDA */
                      free((*pp)->vet[i]);   /* ou  *((*pp)->vet + i)  */
                      free((*pp)->vet);   /*libera o vetor*/
		      free(*pp);    /*libera o descritor*/
		      *pp=NULL;  /*anula o ponteiro passado por referencia: pilha n�o criada */
                      i = capMax;                               /* para quebrar o la�o for mais externo */
                      ret = FRACASSO;
                 }
       }
       else
         {  free(*pp);    /*libera o descritor*/
            *pp = NULL;   /*anula o ponteiro passado por referencia: pilha n�o criada */
 	    ret = FRACASSO;
         }
    }
    else
        ret = FRACASSO;
  return ret;
}

void destroi(ppPE pp) 
{ int i;
   for(i = 0; i< (*pp)->tamVet; i++)  /*libera mem�ria que inicializa os ponteiros void*/
     free( (*pp)->vet[i]);   /* equivalente a: *(p->vet + i));*/
  free((*pp)->vet);
  free(*pp);
  *pp = NULL;
}


/*-------------------------------------------------------------------------------*/
void purga(pPE p) 
{
   p->topo = -1;
}
/*-----------------------------------------------------------------------------*/
int buscaNoTopo(pPE p, void * pReg) {
 int ret;
 if (p->topo == VAZIA) /* acesso direto ao topo */
   ret = FRACASSO;
 else
 {
   memcpy(pReg, (p->vet[p->topo]), p->tamInfo);// equivalente a: memcpy(pReg,*(p->vet+p->topo), p->tamInfo);
   ret = SUCESSO;
 }
 return ret;
}

/*------------ATEN��O: pelo conceito de transa��o at�mica
desempilha  #apenas# decrementaria o topo ---------------------*/
int desempilha(pPE p)
{ if (p->topo == VAZIA)
       return FRACASSO;
  else
  {  /* memcpy(pReg, *(p->vet+p->topo) , p->tamInfo); */
   p->topo--;
      return SUCESSO;
   }
}

/*------------------------------------------------------------------------------*/
int empilha(pPE p, void * novo)
{ if (p->topo < p->tamVet-1)
   { 
       p->topo++;
        /*c�pia um bloco de tamanho definido deste o endere�o novo at� o endere�o 
	definido por *(p->vet+p->topo) */
       memcpy(*(p->vet+p->topo), novo, p->tamInfo); /* equivalente a: memcpy(p->vet[p->topo],novo,p->taminfo);*/
       return SUCESSO;
    }
    else
return FRACASSO;
}


/*--------------------------------------------------------------------------------*/
int testaVazia(pPE p) {
    int ret;
    if (p->topo == VAZIA) /* acesso direto ao topo */
       ret = SIM;
    else
       ret = NAO;
    return ret;
}

/*---------------------------------------------------------------------------------*/
int testaCheia(pPE p) {
  int ret;
  if (p->topo >= p->tamVet-1) /* acesso direto ao topo */
    ret = SIM;
  else
    ret = NAO;
return ret;
}

