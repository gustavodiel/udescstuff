/*#include "tda_ve~1.h"*/
#include "tdaPilhaPriv.h"


int cria(ppPE pp, int capMax)
{ int i, ret = SUCESSO;
   if (  ( *pp = (pPE) malloc(sizeof(tipoDescPE)) )!= NULL )             /* cria header */
   {  (*pp)->vet = (void *) malloc(capMax * sizeof(NoVet));     /* cria vetor */
       if ( (*pp)->vet !=NULL )
       { (*pp)->tamVet = capMax;             /*inicia tamanho do vetor*/
         /*(*pp)->tamInfo = tamInfo; */          /*inicia pilha vazia */
         (*pp)->topo = -1;                   /*pilha vazia, o topo n�o indexa o vetor*/
         for(i=0;i < capMax ;i++)            /*aloca mem�ria para inicializar os ponteiros void*/
           {
             (*pp)->vet[i].p=NULL;
             (*pp)->vet[i].tamInfo=-1;
           }
       }
       else
         {  free(*pp);    /*libera o descritor*/
            *pp = NULL;   /*anula o ponteiro passado por referencia: pilha n�o criada */
 	    ret = FRACASSO;
         }
    }
    else
        ret = FRACASSO;
  return ret;
}

void destroi(ppPE pp)
{ int i;
   for(i = 0; i< (*pp)->tamVet; i++)  /*libera mem�ria que inicializa os ponteiros void*/
     {if ((*pp)->vet[i].p!=NULL)
            free( (*pp)->vet[i].p);   /* equivalente a: *(p->vet + i));*/
     }
  free((*pp)->vet);
  free(*pp);
  *pp = NULL;
}


/*-------------------------------------------------------------------------------*/
void purga(pPE p)
{
   p->topo = -1;
}
/*-----------------------------------------------------------------------------*/
int buscaNoTopo(pPE p, void * pReg) {
 int ret;
 if (p->topo == VAZIA) /* acesso direto ao topo */
   ret = FRACASSO;
 else
 {
   memcpy(pReg, (p->vet[p->topo].p), p->vet[p->topo].tamInfo);// equivalente a: memcpy(pReg,*(p->vet+p->topo), p->tamInfo);
   ret = SUCESSO;
 }
 return ret;
}

/*------------ATEN��O: pelo conceito de transa��o at�mica
desempilha  #apenas# decrementaria o topo ---------------------*/
int desempilha(pPE p) /*pReg  p)*/
{ if (p->topo == VAZIA)
       return FRACASSO;
  else
  {  /* memcpy(pReg, *(p->vet+p->topo) , p->vet[p->topo].tamInfo); */
   p->topo--;
      return SUCESSO;
   }
}

/*------------------------------------------------------------------------------*/
int empilha(pPE p, void * novo, int tamInfo)
{ if (p->topo < p->tamVet-1)
   {
       p->topo++;
       p->vet[p->topo].tamInfo=tamInfo;
       if (p->vet[p->topo].p!=NULL)
           free(p->vet[p->topo].p);
       if ((p->vet[p->topo].p=malloc(tamInfo))==NULL); // verificar aloca��o e tratar no caso de falha
       {
           p->topo--;
           return FRACASSO;
       }
        /*c�pia um bloco de tamanho definido deste o endere�o novo at� o endere�o
	definido por *(p->vet+p->topo) */
       memcpy(p->vet[p->topo].p, novo, tamInfo); /* equivalente a: memcpy(p->vet[p->topo],novo,p->taminfo);*/
       return SUCESSO;
    }
    else
return FRACASSO;
}


/*--------------------------------------------------------------------------------*/
int testaVazia(pPE p) {
    int ret;
    if (p->topo == VAZIA) /* acesso direto ao topo */
       ret = SIM;
    else
       ret = NAO;
    return ret;
}

/*---------------------------------------------------------------------------------*/
int testaCheia(pPE p) {
  int ret;
  if (p->topo >= p->tamVet-1) /* acesso direto ao topo */
    ret = SIM;
  else
    ret = NAO;
return ret;
}

