print "Digite o numero de linhas a repetir"
local coisa = io.read()
while not tonumber(coisa) do
    coisa = io.read()
end

for i = 1, tonumber(coisa) do
    local a = io.read()
    local p1, p2 = string.match(a, "(.*) (.*)");
    if p1 and p2 then
        if string.sub(string.reverse(p1), 1, #p2) == string.reverse(p2) then
            print "Encaixa"
        else
            print "Nao encaixa"
        end
    end
end

