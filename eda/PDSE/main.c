#include "PDSE.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct eita
{
	int a,b,c;
} eoq;

int main(void) { 
	pPDSE p=NULL; //<<<< atencao: essa declaracao tem que ser com o typedef
	eoq jao;
	jao.a = 1;
	jao.b = 1;
	jao.c = 2;
	if (cria(&p,sizeof(struct eita))){
		insere(p, &jao);
		busca(p, &jao);
		printf("topo: {a=%d, b=%d, c=%d}\n",jao.a, jao.b, jao.c);
		eoq cuzao;
		cuzao.a = 0;
		cuzao.b = 23;
		cuzao.c = 24;
		insere(p, &cuzao);
		busca(p, &jao);
		printf("topo: {a=%d, b=%d, c=%d}\n",jao.a, jao.b, jao.c);
	}
}


