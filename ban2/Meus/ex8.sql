-- 1)      Gatilho para impedir a inserção ou atualização de Clientes com o mesmo CPF.
CREATE OR REPLACE FUNCTION exer1() RETURNS TRIGGER AS $$
DECLARE
BEGIN
	IF (SELECT 1 FROM cliente WHERE cpf = NEW.cpf) THEN
    	RAISE EXCEPTION 'Não pode criar cliente com cpf igual!!!';
    END IF;
    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER tExer1 BEFORE INSERT OR UPDATE ON cliente FOR EACH ROW EXECUTE PROCEDURE exer1();

INSERT INTO cliente (codc, cpf, nome) values (9, '20000200000',  'Fulano');

-- 2)      Gatilho para impedir a inserção ou atualização de Mecânicos com idade menor que 20 anos.
CREATE OR REPLACE FUNCTION exer2() RETURNS TRIGGER AS $$
DECLARE
BEGIN
	IF (NEW.idade < 20) THEN
    	RAISE EXCEPTION 'Não pode mexer com mecanico com meno de 20 anos';
    END IF;
    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

DROP TRIGGER tExer2 ON mecanico;

CREATE TRIGGER tExer2 BEFORE INSERT OR UPDATE ON mecanico FOR EACH ROW EXECUTE PROCEDURE exer2();


-- 3)      Gatilho para atribuir um cods (sequencial) para um novo setor inserido.
CREATE SEQUENCE setor_cod_seq START 7;
CREATE OR REPLACE FUNCTION exer3() RETURNS TRIGGER AS $$
BEGIN
	NEW.cods := nextval('setor_cod_seq');
    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER tExer3 BEFORE INSERT ON setor FOR EACH ROW EXECUTE PROCEDURE exer3();


-- 4)      Gatilho para impedir a inserção de um mecânico ou cliente com CPF inválido.
create or replace function valida_cpf() returns TRIGGER as
$$
declare
	vc char;
    vi int default 1;
    vsuma1 int default 0;
    vresto int;
    vdig char(1);
    vresult boolean default true;
    pcpf char(11);
begin
    pcpf := NEW.cpf;
	while (vi < 10) loop
        vsuma1 := vsuma1 + (11 - vi) * cast( substr(pcpf, vi, 1) as integer);
        vi := vi +1;
    end loop;
    vresto := vsuma1 % 11;
    if( vresto < 2) then
    	vdig = '0';
    else
    	vdig = cast( 11 - vresto as char(1));
    end if;
    if(substr(pcpf, 10, 1) != vdig) then
    	vresult := false;
    end if;
    
    vi := 1;
    vsuma1 := 0;
    
    WHILE ( vi < 11) LOOP
        vsuma1 := vsuma1 + (12 - vi) * cast( substr(pcpf, vi, 1) as integer);
        vi := vi +1;
    END LOOP;
    vresto := vsuma1 % 11;
    if( vresto < 2) then
    	vdig = '0';
    else
    	vdig = cast( 11 - vresto as char(1));
    end if;
    if(substr(pcpf, 11, 1) != vdig) then
    	vresult := false;
    end if;
    
    IF vresult THEN
        return NEW;
    END IF;
    raise notice 'CPF Invalido!';
    return NULL;
end;
$$
language plpgsql;

CREATE TRIGGER tExer4 BEFORE INSERT ON cliente FOR EACH ROW EXECUTE PROCEDURE valida_cpf();


-- 5)      Gatilho para impedir que um mecânico seja removido caso não exista outro mecânico com a mesma função.
CREATE OR REPLACE FUNCTION exer5() RETURNS TRIGGER AS $$
DECLARE
    numFunc int;
BEGIN
    SELECT COUNT(1) INTO numFunc FROM mecanico WHERE funcao = OLD.funcao;
    if numFunc = 1 THEN
        RAISE NOTICE 'Precisamos desse cara!';
        RETURN NULL;
    END IF;
    RETURN OLD;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER tExer5 BEFORE DELETE ON mecanico FOR EACH ROW EXECUTE PROCEDURE exer5();
-- 6)      Gatilho que ao inserir, atualizar ou remover um mecânico, reflita as mesmas modificações na tabela de Cliente. Em caso de atualização, se o mecânico ainda não existir na tabela de Cliente, deve ser inserido.
CREATE OR REPLACE FUNCTION exer6() RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'UPDATE' THEN
    	IF (SELECT COUNT(1) FROM cliente WHERE cpf = NEW.cpf GROUP BY cpf) <> 0 THEN -- Existe um cliente
        	UPDATE cliente SET idade = NEW.idade, endereco = NEW.endereco, cidade = NEW.cidade WHERE cpf = NEW.cpf;
        ELSE
        	INSERT INTO cliente (codc, cpf, nome, idade, endereco, cidade) VALUES 
            	(NEW.codm, NEW.cpf, NEW.nome, NEW.idade, NEW.endereco, NEW.cidade);
        END IF;
    ELSIF TG_OP = 'INSERT' THEN
    	INSERT INTO cliente (codc, cpf, nome, idade, endereco, cidade) VALUES 
            	(NEW.codm, NEW.cpf, NEW.nome, NEW.idade, NEW.endereco, NEW.cidade);
    ELSE
    	DELETE FROM cliente
        WHERE cpf = OLD.cpf;
        RETURN OLD;
    END IF;
    
    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER tExer6 BEFORE DELETE OR UPDATE OR INSERT ON mecanico FOR EACH ROW EXECUTE PROCEDURE exer6();

-- 7)      Gatilho para impedir que um conserto seja inserido na tabela Conserto se o mecânico
--          já realizou mais de 20 horas extras no mês.

CREATE OR REPLACE FUNCTION exer7() RETURNS TRIGGER AS $$
BEGIN
    IF (SELECT COUNT(1) FROM conserto c WHERE NEW.codm = c.codm AND date_part('month', c.data) = date_part('month', NEW.data) AND date_part('year', c.data) = date_part('year', NEW.data)) > 180 THEN
    	RAISE EXCEPTION 'Poxa, da um break pro cara mano';
        RETURN NULL;
    END IF;
    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

DROP TRIGGER tExer7 ON conserto;

CREATE TRIGGER tExer7 BEFORE INSERT ON conserto FOR EACH ROW EXECUTE PROCEDURE exer7();

-- 8)

CREATE OR REPLACE FUNCTION exer8() RETURNS TRIGGER AS $$
DECLARE
	vMecanico mecanico%rowtype;
BEGIN
	SELECT * INTO vMecanico FROM mecanico WHERE codm = NEW.codm;
    
    IF (SELECT COUNT(1) FROM conserto c JOIN mecanico m ON c.codm = m.codm
        	WHERE c.data = NEW.data AND c.hora = NEW.hora
       		AND m.cods = vMecanico.cods
       ) > 0 THEN
    	RAISE EXCEPTION 'Ja tem conserto demais nesse setor :x';
        RETURN NULL;
    END IF;
    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

DROP TRIGGER tExer8 ON conserto;

CREATE TRIGGER tExer8 BEFORE INSERT ON conserto FOR EACH ROW EXECUTE PROCEDURE exer8();
