-- Essa linha é necessária por causa do meu computador.
SET datestyle = "ISO, DMY";

-- Trabalho de Gustavo Diel

-- 1
SELECT cpf, nome FROM cliente;

-- 2
SELECT nome, funcao FROM mecanico WHERE cods = 2;


-- 3
SELECT cpf, nome, idade FROM cliente
INTERSECT
SELECT cpf, nome, idade FROM mecanico;

-- 4
SELECT DISTINCT cidade FROM cliente JOIN mecanico USING(cidade);

-- 5
SELECT DISTINCT modelo FROM veiculo JOIN cliente USING(codc)
WHERE cidade = 'Joinville';

-- 6
SELECT DISTINCT funcao FROM mecanico;

-- 7
SELECT * FROM cliente
WHERE idade > 25;

-- 8
SELECT cpf, mecanico.nome FROM mecanico JOIN setor USING(cods)
WHERE setor.nome = 'Mecânica';

-- 9
SELECT nome FROM mecanico JOIN conserto USING(codm)
WHERE data = '13/06/2014';

-- 10
SELECT DISTINCT c.nome AS nome_cli, c.cpf AS cli_cpf, v.modelo, m.nome AS nome_mec, m.funcao
FROM cliente c JOIN veiculo v USING(codc) JOIN conserto con USING (codv) JOIN mecanico m USING (codm);

-- 11
SELECT m.nome AS nome_mec, c.nome as nome_cli, hora FROM conserto JOIN mecanico m USING(codm) JOIN veiculo USING(codv)
JOIN cliente c USING(codc)
WHERE data ='19/06/2014';

-- 12
SELECT s.cods AS setor, s.nome FROM setor s JOIN mecanico USING (cods) JOIN conserto USING(codm)
WHERE data >= '12/06/2014' AND data <= '14/06/2014'


-- 
