-- Essa linha é necessária por causa do meu computador.
SET datestyle = "ISO, DMY";

-- Trabalho de Gustavo Diel


-- 1) Recupere o CPF e o nome dos mecânicos que trabalham nos setores número 1 e 2 (faça a consulta utilizado a cláusula IN).
SELECT m.cpf, m.nome FROM mecanico m WHERE m.cods IN (1, 2);

-- 2) Recupere o CPF e o nome dos mecânicos que trabalham nos setores 'Funilaria' e 'Pintura' (faça a consulta utilizando sub-consultas aninhadas).
SELECT m.cpf, m.nome FROM mecanico m WHERE cods IN (
	SELECT cods FROM setor s WHERE s.nome IN ('Funilaria', 'Pintura'));

-- 3) Recupere o CPF e nome dos mecânicos que atenderam no dia 13/06/2014 (faça a consulta usando INNER JOIN).
SELECT m.cpf, m.nome FROM mecanico m JOIN conserto c USING (codm)
WHERE c.data = '13/06/2014';

-- 4) Recupere o nome do mecânico, o nome do cliente e a hora do conserto para os consertos realizados no dia 12/06/2014 (faça a consulta usando INNER JOIN).
SELECT m.nome AS nome_mec, c.nome AS nome_cli, con.hora FROM mecanico m JOIN conserto con USING (codm) JOIN veiculo v USING (codv) JOIN cliente c USING (codc)
WHERE con.data = '12/06/2014';

-- 5) Recupere o nome e a função de todos os mecânicos, e o número e o nome dos setores para os mecânicos que tenham essa informação.
SELECT m.nome, m.funcao, s.cods, s.nome FROM mecanico m LEFT JOIN setor s USING (cods);

-- 6) Recupere o nome de todos os mecânicos, e as datas dos consertos para os mecânicos que têm consertos feitos (deve aparecer apenas um registro de nome de mecânico para cada data de conserto).
SELECT DISTINCT m.nome, con.data FROM mecanico m LEFT JOIN conserto con USING (codm) ORDER BY m.nome;

-- 7) Recupere a média da quilometragem de todos os veículos dos clientes.
SELECT AVG(quilometragem) FROM veiculo JOIN cliente USING (codc);

-- 8) Recupere a soma da quilometragem dos veículos de cada cidade onde residem seus proprietários.
SELECT SUM(quilometragem), cidade FROM veiculo JOIN cliente USING (codc) GROUP BY (cidade);

-- 9) Recupere a quantidade de consertos feitos por cada mecânico durante o período de 12/06/2014 até 19/06/2014
SELECT COUNT(*), m.nome FROM mecanico m JOIN conserto USING (codm) GROUP BY m.nome;

-- 10) Recupere a quantidade de consertos feitos agrupada pela marca do veículo.
SELECT COUNT(*), marca FROM veiculo GROUP BY marca;

-- 11) Recupere o modelo, a marca e o ano dos veículos que têm quilometragem maior que a média de quilometragem de todos os veículos.
SELECT v.modelo, v.marca, v.ano, v.quilometragem FROM veiculo v
WHERE v.quilometragem > (SELECT AVG(quilometragem) FROM veiculo);

-- 12) Recupere o nome dos mecânicos que têm mais de um conserto marcado para o mesmo dia.
SELECT m.nome, c.data, COUNT(1) as d FROM mecanico m JOIN conserto c USING (codm) GROUP BY c.data, nome, c.data
HAVING COUNT(1) > 1
