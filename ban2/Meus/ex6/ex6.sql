-- 1)      Mostre o nome e a função dos mecânicos.
CREATE VIEW mecanico_nome_func (nome, funcao) AS
SELECT m.nome, m.funcao
FROM mecanico m;

SELECT * FROM mecanico_nome_func;

-- 2)      Mostre o modelo e a marca dos veículos dos clientes.
CREATE VIEW vei_modelo_marca (modelo, marca) AS
SELECT v.modelo, v.marca
FROM veiculo v
GROUP BY (v.modelo, v.marca)
ORDER BY (v.marca);

SELECT * FROM vei_modelo_marca;

-- 3)      Mostre o nome dos mecânicos, o nome dos clientes, o modelo dos veículos e a data e hora dos consertos realizados.
CREATE VIEW calendario_consertos (nome_mec, nome_cli, modelo, data, hora) AS
SELECT m.nome, c.nome, v.modelo, con.data, con.hora
FROM conserto con
	JOIN mecanico m USING (codm)
    JOIN veiculo v USING (codv)
    JOIN cliente c USING (codc);

SELECT * FROM calendario_consertos;

-- 4)      Mostre o ano dos veículos e a média de quilometragem para cada ano.
CREATE VIEW ano_km_veiculos (ano, media_quilometragem) AS
SELECT v.ano, AVG(v.quilometragem)
FROM veiculo v
GROUP BY v.ano
ORDER BY v.ano DESC;

SELECT * FROM ano_km_veiculos;

-- 5)      Mostre o nome dos mecânicos e o total de consertos feitos por um mecânico em cada dia.
CREATE VIEW nome_total_con_dia_mec (nome_mec, dia_conserto, total_consertos_por_dia) AS
SELECT m.nome, con.data, COUNT(1)
FROM mecanico m
	JOIN conserto con USING(codm)
GROUP BY (m.nome, con.data)
ORDER BY m.nome;

SELECT * FROM nome_total_con_dia_mec;

-- 6)      Mostre o nome dos setores e o total de consertos feitos em um setor em cada dia.
CREATE VIEW nome_set_total_con_dia (nome_set, dia_conserto, total_consertos_por_dia) AS
SELECT s.nome, con.data, COUNT(1)
FROM setor s
	JOIN mecanico m USING(cods)
    JOIN conserto con USING(codm)
GROUP BY (s.nome, con.data)
ORDER BY (s.nome);

SELECT * FROM nome_set_total_con_dia

-- 7)      Mostre o nome das funções e o número de mecânicos que têm uma destas funções.
CREATE VIEW func_quant_mec (nome_funcao, numero_mec) AS
SELECT m.funcao, COUNT(1)
FROM mecanico m
GROUP BY(m.funcao);

SELECT * FROM func_quant_mec


-- 8)      Mostre o nome dos mecânicos e suas funções e, para os mecânicos que estejam alocados a um setor, informe também o número e nome do setor.
CREATE VIEW mec_funcao (nome_mec, funcao, num_setor, nome_setor) AS
SELECT m.nome, m.funcao, s.cods, s.nome
FROM mecanico m LEFT JOIN setor s USING (cods);

SELECT * FROM mec_funcao;

-- 9)      Mostre o nome das funções dos mecânicos e a quantidade de consertos feitos agrupado por cada função.
CREATE VIEW quant_conserto_funcao (funcao, quant) AS
SELECT m.funcao, COUNT(1)
FROM mecanico m
	JOIN conserto c using (codm)
GROUP BY m.funcao;
SELECT * FROM quant_conserto_funcao
