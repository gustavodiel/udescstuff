-- Gustavo Diel

-- 1)    Recupere todos os atributos dos professores do departamento Ciência da Computação.
SELECT p.*
FROM professor p
	JOIN departamento d ON p.codigoDepartamento = d.codigo
WHERE UPPER(d.nome) = 'CIÊNCIA DA COMPUTAÇÃO';

-- 2)    Recupere o CPF, nome e endereço dos professores do departamento Administração.
SELECT p.cpf, p.nome, p.logradouro, p.cidade, p.cep
FROM professor p
	JOIN departamento d ON p.codigoDepartamento = d.codigo
WHERE UPPER(d.nome) = 'ADMINISTRAÇÃO';

-- 3)    Recupere as disciplinas dos cursos de Ciência da Computação e Física.
SELECT d.*
FROM disciplina d
	JOIN curso c ON c.codigo = d.codigoCurso
WHERE UPPER(c.nome) IN ('CIÊNCIA DA COMPUTAÇÃO', 'FISICA');

-- 4)    Recupere os alunos matriculados na disciplina Engenharia de Software do curso Ciência da Computação no
--		 	semestre 2012-2. O resultado deve conter: ano, semestre, matrícula do aluno, nome do aluno, código da disciplina,
--			nome da disciplina e o código do curso.
SELECT m.anosemestre, m.numerosemestre, m.matriculaaluno, a.nome, d.codigo, d.nome, c.codigo
FROM aluno a
	JOIN matricula m ON m.matriculaaluno = a.matricula
	JOIN disciplina d ON m.codigodisciplina = d.codigo
    JOIN curso c ON c.codigo = d.codigocurso
WHERE UPPER(d.nome) = 'ENGENHARIA DE SOFTWARE'
	AND UPPER(c.nome) = 'CIÊNCIA DA COMPUTAÇÃO'
    AND m.anosemestre = 2012
    AND m.numerosemestre = 2;

-- 5)    Recupere a quantidade de alunos matriculados na disciplina Banco de Dados II do curso Ciência da Computação em cada
--			semestre letivo.
SELECT COUNT(a.*), m.anosemestre, m.numerosemestre
FROM aluno a
	JOIN matricula m ON (a.matricula =  m.matriculaaluno)
    JOIN disciplina d ON (m.codigodisciplina = d.codigo AND m.codigocurso = d.codigocurso)
    JOIN curso c ON (d.codigocurso = c.codigo AND m.codigocurso = c.codigo)
WHERE UPPER(d.nome) = 'BANCO DE DADOS II'
	AND UPPER(c.nome) = 'CIÊNCIA DA COMPUTAÇÃO'
GROUP BY m.numerosemestre, m.anosemestre;

-- 6)    Recupere o CPF e nome dos professores que reprovaram mais de 2 alunos em um semestre (reprovação => nota < 7.0).
--			Mostrar também a ano e semestre das reprovações.
SELECT p.cpf, p.nome, m.anosemestre, m.numerosemestre
FROM matricula m
    JOIN vinculo v ON v.codigodisciplina = m.codigodisciplina AND v.codigocurso = m.codigocurso
    JOIN professor p ON p.cpf = v.cpfprofessor
WHERE m.notafinal < 7
GROUP BY p.cpf, m.anosemestre, m.numerosemestre
HAVING COUNT(1) > 2;

-- 7)    Recupere o código da disciplina, nome da disciplina, código do curso, número de alunos matriculados na disciplina e
--			a média das notas dos alunos matriculados na disciplina.
SELECT d.codigo, d.nome, c.codigo, COUNT(1) AS qntAlunos, AVG(m.notafinal) as mediaNotas
FROM matricula m
    JOIN disciplina d ON (m.codigodisciplina = d.codigo AND m.codigocurso = d.codigocurso)
    JOIN curso c ON (d.codigocurso = c.codigo AND m.codigocurso = c.codigo)
GROUP BY d.codigo, d.nome, c.codigo;


-- 8)    Recupere os professores do curso de Ciência da Computação, em ordem alfabética. A lista deve conter todos os atributos
--			de Professor e o código do departamento onde ele está vinculado.
SELECT DISTINCT p.*
FROM professor p
	JOIN vinculo v ON (p.cpf = v.cpfprofessor)
    JOIN departamento dep ON dep.codigo = p.codigodepartamento
    JOIN curso c ON (c.codigo = v.codigocurso)
WHERE UPPER(c.nome) = 'CIÊNCIA DA COMPUTAÇÃO'
ORDER BY p.nome;

-- 9)    Recupere os alunos que tiveram faltas. A lista deve conter nome do curso, nome da disciplina, nome do aluno e número
--			de faltas. Ordenar a relação de forma decrescente, de acordo com número de faltas.
SELECT c.nome, d.nome, a.nome, COUNT(f.dataaula)
FROM aluno a
	JOIN matricula m ON a.matricula = m.matriculaaluno
    JOIN disciplina d ON (m.codigodisciplina = d.codigo AND m.codigocurso = d.codigocurso)
    JOIN curso c ON (d.codigocurso = c.codigo)
    JOIN frequencia f ON a.matricula = f.matriculaaluno
WHERE
	f.indpresenca = 'N'
GROUP BY c.nome, d.nome, a.nome
ORDER BY COUNT(f.dataaula) DESC, a.nome DESC
