1)      Crie um índice para cada uma das chaves estrangeiras presentes do esquema de dados.
CREATE INDEX idx_mec_ex1 ON mecanico USING btree(cods);
DROP INDEX idx_mec_ex1;

CREATE INDEX idx_cli_ex1 ON cliente (codc);
DROP INDEX idx_cli_ex1;

CREATE INDEX idx_con_ex1 ON conserto (codm, codm);
DROP INDEX idx_con_ex1;

CREATE INDEX idx_set_ex1 ON setor (cods);
DROP INDEX idx_set_ex1;

CREATE INDEX idx_veh_ex1 ON veiculo (codv);
DROP INDEX idx_veh_ex1;

2)      Crie um índice para acelerar a busca dos mecânicos pela função.
CREATE INDEX idx_mec_ex2 ON mecanico USING hash(funcao);
DROP INDEX idx_mec_ex2;

3)      Crie um índice para acelerar a ordenação dos consertos pela data e hora.
CREATE INDEX idx_con_ex3 ON conserto USING btree(data, hora);
DROP INDEX idx_con_ex3;

4)      Crie um índice para acelerar a busca de clientes pelo cpf.
CREATE INDEX idx_cli_ex4 ON cliente USING btree(cpf);
DROP INDEX idx_cli_ex4;

5)      Crie um índice para acelerar a busca pelas primeiras 5 letras do nome dos clientes.
CREATE INDEX idx_cli_ex5 ON cliente USING btree(substr(nome, 1, 5));
DROP INDEX idx_cli_ex5;

6)      Crie um índice para acelerar a busca dos clientes com CPF com final XXX.XXX.XXX-55.
CREATE INDEX idx_cli_ex6 ON cliente USING btree(cpf) WHERE CAST(cpf as text) LIKE '_________55';
DROP INDEX idx_cli_ex6;