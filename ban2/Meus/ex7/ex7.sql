-- 1)
CREATE OR REPLACE FUNCTION insere_exclue_setor(pacao int, pcods integer, pnome varchar) RETURNS int AS
$$
DECLARE
    vlinhas int default 0;
begin
    if (pacao = 1) then
        INSERT INTO setor VALUES (pcods, pnome);
    elsif(pacao = 2) then
        DELETE FROM setor WHERE cods = pcods AND nome = pnome;
    else
        RAISE NOTICE 'Acao Desconhecida!';
    end if;
    GET DIAGNOSTICS vlinhas = row_count;
    return vlinhas;
end;
$$
LANGUAGE plpgsql;

-- 2)
CREATE OR REPLACE FUNCTION insere_exclue_mecanico(pacao int, pcodm integer, pnome varchar, pidade int, pendereco varchar, pcidade varchar, pfuncao varchar, pcods int) RETURNS int AS
$$
DECLARE
    vlinhas int default 0;
begin
    if (pacao = 1) then
        INSERT INTO mecanico VALUES (pcodm, pnome, pidade, pendereco, pcidade, pfuncao, pcods);
    elsif(pacao = 2) then
        DELETE FROM mecanico WHERE codm = pcodm;
    else
        RAISE NOTICE 'Acao Desconhecida!';
    end if;
    GET DIAGNOSTICS vlinhas = row_count;
    return vlinhas;
end;
$$
LANGUAGE plpgsql;

-- 3)
CREATE OR REPLACE FUNCTION insere_exclue_cliente(pacao int, pcodc integer, cpf varchar, pnome varchar, pidade int, pendereco varchar, pcidade varchar) RETURNS int AS
$$
DECLARE
    vlinhas int default 0;
begin
    if (pacao = 1) then
        INSERT INTO cliente VALUES (pcodm, pcpf, pnome, pidade, pendereco, pcidade);
    elsif(pacao = 2) then
        DELETE FROM cliente WHERE codc = pcodc;
    else
        RAISE NOTICE 'Acao Desconhecida!';
    end if;
    GET DIAGNOSTICS vlinhas = row_count;
    return vlinhas;
end;
$$
LANGUAGE plpgsql;

-- 4)
CREATE OR REPLACE FUNCTION insere_exclue_veiculo(pacao int, pcodv integer, renavam varchar, modelo varchar, marca varchar, ano int, quilometragem int, codc int) RETURNS int AS
$$
DECLARE
    vlinhas int default 0;
begin
    if (pacao = 1) then
        INSERT INTO veiculo VALUES (pcodv, renavam, modelo, marca, ano, quilometragem, codc);
    elsif(pacao = 2) then
        DELETE FROM veiculo WHERE codv = pcodv;
    else
        RAISE NOTICE 'Acao Desconhecida!';
    end if;
    GET DIAGNOSTICS vlinhas = row_count;
    return vlinhas;
end;
$$
LANGUAGE plpgsql;


-- 5)
CREATE OR REPLACE FUNCTION insere_exclue_conserto(pacao int, pcodm integer, pcodv integer, pdata date, phora time) RETURNS int AS
$$
DECLARE
    vlinhas int default 0;

begin
    if (pacao = 1) then
        INSERT INTO conserto VALUES (pcodm, pcodv, pdata, phora);
    elsif(pacao = 2) then
        DELETE FROM conserto WHERE codm = pcodm AND codv = pcodv AND data = pdata;
    else
        RAISE NOTICE 'Acao Desconhecida!';
    end if;
    GET DIAGNOSTICS vlinhas = row_count;
    return vlinhas;
end;
$$
LANGUAGE plpgsql;

SELECT insere_exclue_conserto(3, 3, 4, '12/09/2017', null);

-- 6) Função para calcular a média geral de idade dos Mecânicos e Clientes.
CREATE OR REPLACE FUNCTION media_mecanicos_clientes() RETURNS float AS
$$
DECLARE
	vmediacli float default 0;
    vmediamec float default 0;
    vmediafin float default 0;
BEGIN
	SELECT AVG(idade) INTO vmediacli FROM cliente;
    SELECT AVG(idade) INTO vmediamec FROM mecanico;
    vmediafin = (vmediacli + vmediamec) / 2;
    RETURN vmediafin;
end;
$$
LANGUAGE plpgsql;

SELECT media_mecanicos_clientes();

-- 7)
CREATE OF REPLACE FUNCTION del_set_mec_cli_vec(ptipo int, pcod int) RETURNS void AS
$$
DECLARE
	vlinhas int default 0;
BEGIN
	IF (ptipo = 1) THEN
    	DELETE FROM setor WHERE cods = pcod;
    ELSE IF (ptipo = 2) THEN
    	DELETE FROM mecanico WHERE codm = pcod;
    ELSE IF (ptipo = 3) THEN
    	DELETE FROM veiculo WHERE codv = pcod;
    ELSE IF (ptipo = 4) THEN
    	DELETE FROM cliente WHERE codc = pcod;
    ELSE
    	RAISE NOTICE 'Acao desconhecida!'
    END IF;
    GET DIAGNOSTICS vlinhas = row_count;
   	RETURN vlinhas;
END;
$$
LANGUAGE plpgsql;
    
    
-- 8)
CREATE OR REPLACE FUNCTION exclui_cpf_repetido() RETURNS int AS
$$
DECLARE
	vcliente cliente%rowtype;
    vfoi boolean default false;
BEGIN
	FOR vcliente IN SELECT * FROM cliente GROUP BY codc, cpf HAVING COUNT(1) > 1 LOOP
        if (vfoi) THEN
            DELETE FROM cliente c WHERE c.codc = vcliente.codc;
        END IF;
        vfoi := true;
    END LOOP;
   	RETURN vlinhas;
END;
$$
LANGUAGE plpgsql;

-- 9)   Função para calcular se o CPF é válido*.
create or replace function valida_cpf( pcpf char(11)) returns boolean as
$$
declare
	vc char;
    vi int default 1;
    vsuma1 int default 0;
    vresto int;
    vdig char(1);
    vresult boolean default true;
begin
	-- Verifica primeiro digito
	while (vi < 10) loop	-- primeira verificação
        -- raise notice '1>>pcpf[%] é %', vi, cast( substr(pcpf, vi, 1) as integer);
        vsuma1 := vsuma1 + (11 - vi) * cast( substr(pcpf, vi, 1) as integer);
        vi := vi +1;
    end loop;
    vresto := vsuma1 % 11;
    if( vresto < 2) then
    	vdig = '0';
    else
    	vdig = cast( 11 - vresto as char(1));
    end if;
    raise notice '1>>suma: %, resto: %, digito: %', vsuma1, vresto, vdig;
    if(substr(pcpf, 10, 1) != vdig) then
    	vresult := false;
    end if;
    
    vi := 1;
    vsuma1 := 0;
    
    -- Verifica segundo digito
    while ( vi < 11) loop
    	-- raise notice '2>>pcpf[%] é %', vi, cast( substr(pcpf, vi, 1) as integer);
        vsuma1 := vsuma1 + (12 - vi) * cast( substr(pcpf, vi, 1) as integer);
        vi := vi +1;
    end loop;
    vresto := vsuma1 % 11;
    if( vresto < 2) then
    	vdig = '0';
    else
    	vdig = cast( 11 - vresto as char(1));
    end if;
    raise notice '1>>suma: %, resto: %, digito: %', vsuma1, vresto, vdig;
    if(substr(pcpf, 11, 1) != vdig) then
    	vresult := false;
    end if;
    
    return vresult;
end;
$$
language plpgsql;
-- 10)   Função que calcula a quantidade de horas extras de um mecânico em um mês
--      de trabalho. O número de horas extras é calculado a partir das horas que
--      excedam as 160 horas de trabalho mensais. O número de horas mensais
--      trabalhadas é calculada a partir dos consertos realizados.
--      Cada conserto tem a duração de 1 hora.

create or replace function calcual_hora_extra( pcodm integer, pmes int) returns  time without time zone as
$$
declare
	vhoratrab time without time zone default 0;
begin
	select sum(hora) into vhoratrab from conserto where codm = pcodm and date_part('month', data) = pmes group by codm, date_part('month', data);
    if( vhoratrab > 0) then
    	return;
end;
$$
language plpgsql;