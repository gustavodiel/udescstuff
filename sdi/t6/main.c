#include <stdio.h>
#include <string.h>
#include <rpc/rpc.h>
#include "hw.h"
#include <pthread.h>

typedef struct enviaInfo {
    CLIENT* client;
    char user[50];
} enviaInfo;

typedef struct recebeInfo {
    CLIENT *client;
    int numMsg;
} recebeInfo;

void *enviaMensagem(void *v_ptr){

    enviaInfo* dados = (enviaInfo*)v_ptr;

    char user[50];
    strcpy(user, dados->user);
    CLIENT *client = dados->client;

    char *msg = malloc(sizeof(char) * 512);
    char newMsg[512];
    while (1){
        strcpy(msg, user);
        strcat(msg, ":");
        scanf("%s", &newMsg);
        strcat(msg, newMsg);
        printf("%s\n", msg);
        int *r = send_1(&msg, client);
        puts("Passou");
        if (!r){
            clnt_perror(client, "Vesh!");
            return -1;
        }
    }
    return 1;
}

void *recebeMensagem(void *v_ptr){

    recebeInfo* dados = (recebeInfo*)v_ptr;

    int numMsg = dados->numMsg;
    CLIENT *client = dados->client;

    char **msgRecebida;
    while (1){
        msgRecebida = recieve_1(&numMsg, client);
        if (msgRecebida == NULL){
            clnt_perror(client, "Deu m****a!");
            return -1;
        }
        if (strcmp(*msgRecebida, "Vazio") != 0){
            printf(">> %s\n", *msgRecebida);
            numMsg++;
        }
    }
    return 1;
}

int main(int argc, char *argv[]){
    pthread_t envia, recebe;
    int rc, i;
    CLIENT *client;
    char **p;
    int *r;
    char user[50] = "";
    int numMsg = 0;

    char *msg, *lixo;

    msg = (char*)malloc(sizeof(char) * 256);

    if (!msg){
        printf("Deu M****a\n");
        return -1;
    }

    if (argc =! 2){
        printf("Uso: ./cliente localhost\n");
        return -2;
    }

    client =clnt_create(argv[1], DIEL_CHAT, DIEL_CHAT_VERS, "tcp");
    if (!client){
        clnt_pcreateerror(argv[1]);
        return -3;
    }

    p = hw_1(NULL, client);

    if (!p){
        clnt_perror(client, argv[1]);
        return -4;
    }

    printf("%s\n", *p);

    msg = strtok(*p, ":");
    lixo = strtok(*p, ":");

    strcpy(user, *p);

    for( i= 0; i < strlen(msg); i++ ){
        numMsg = numMsg * 10 + (msg[i] - '0');
    }

    enviaInfo *dadosEnvia;
    dadosEnvia->client = client;
    strcpy(dadosEnvia->user, user);

    recebeInfo *dadosRecebe;
    dadosRecebe->client = client;
    dadosRecebe->numMsg = numMsg;

    rc = pthread_create(&envia, NULL, enviaMensagem, (void*)dadosEnvia);
    rc = pthread_create(&recebe, NULL, recebeMensagem, (void*)dadosRecebe);

    pthread_join(envia, NULL);
    pthread_join(recebe, NULL);

    return 0;

}
