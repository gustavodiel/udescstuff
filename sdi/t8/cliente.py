import requests
from threading import Thread
from xml.etree import ElementTree
from time import sleep

''' DEFINICOES ''' 
dataHead = '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><S:Body>'
dataFoot = '</S:Body></S:Envelope>'

headers = {"Content-Type": "text/xml"}

url = "http://localhost:8080/ChatWebService/MyWSClass"
namespaces = {
    'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
    'a': 'http://sdi.diel.com/',
}

myId = 0

def makeConection(content):
    response = requests.post(url, data=dataHead + content +  dataFoot, headers=headers)


    return ElementTree.fromstring(response.text)

def getId(nome):
    dataContent = '<ns2:getId xmlns:ns2="http://sdi.diel.com/"><nome>' + nome + '</nome></ns2:getId>'
    dom = makeConection(dataContent)
    names = dom.findall(
        './soap:Body'
        '/a:getIdResponse'
        '/return',
        namespaces,
    )
    for name in names:
        return name.text

nome = raw_input("Please digite seu nome: ")
meuId = getId(nome)

def sendMessage(msg):
    dataContent = '<ns2:sendMessage xmlns:ns2="http://sdi.diel.com/"><msg>' + msg + '</msg><id>' + meuId + '</id></ns2:sendMessage>'
    dom = makeConection(dataContent)



def listenMessages():
    dataContent = '<ns2:getNewMessages xmlns:ns2="http://sdi.diel.com/"><id>' + meuId + '</id></ns2:getNewMessages>'
    dom = makeConection(dataContent)
    names = dom.findall(
        './soap:Body'
        '/a:getNewMessagesResponse'
        '/return',
        namespaces,
    )
    for name in names:
        if name.text:
            for msg in name.text.split(';'):
                if msg != '' and msg != ' ':
                    print(msg)


class Escuta(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        while True:
            msg = raw_input()
            sendMessage(msg)

class CheckNewMsgs(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        while True:
            sleep(0.5)
            listenMessages()


listenInput = Escuta()
listenInput.start()

checkNewMsgs = CheckNewMsgs()
checkNewMsgs.start()