/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diel.sdi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author gustavo
 */
@WebService(serviceName = "MyWSClass")
public class MyWSClass {
    
    static int numUsuarios = 0;
    
    HashMap<Integer, ArrayList<String>> msgLeft = new HashMap<>();
    HashMap<Integer, String> usuarios = new HashMap();
    

    /**
     * Web service operation
     * @param nome
     * @return 
     */
    @WebMethod(operationName = "getId")
    public Integer getId(@WebParam(name = "nome") final String nome) {
        //TODO write your implementation code here:
        ++numUsuarios;
        msgLeft.put(numUsuarios, new ArrayList<String>());
        usuarios.put(numUsuarios, nome);
        
        System.out.println("Cliente logado: " + nome + ". id: " + String.valueOf(numUsuarios));
        return numUsuarios;
    }

    /**
     * Web service operation
     * @param id
     * @param msg
     * @return 
     */
    @WebMethod(operationName = "sendMessage")
    public String sendMessage(@WebParam(name = "id") Integer id, @WebParam(name = "msg") String msg) {
        
        for (Map.Entry<Integer, ArrayList<String>> entry : msgLeft.entrySet()) {
            Integer key = entry.getKey();
            if (!Objects.equals(key, id)){
                System.out.println("Adiciona msg: " + String.valueOf(key) + " - " + String.valueOf(id));
                ArrayList<String> value = entry.getValue();

                value.add(usuarios.get(id) + ": " + msg);
            }
        }
        return "OK";
    }

    /**
     * Web service operation
     * @param id
     * @return 
     */
    @WebMethod(operationName = "getNewMessages")
    public String getNewMessages(@WebParam(name = "id") Integer id) {
        String send = "";
        for (String s : msgLeft.get(id)){
            send += s + ";";
        }
        msgLeft.get(id).clear();
        return send;
    }
}
