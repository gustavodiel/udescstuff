/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gustavo
 */
public class ClienteUDP {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String args[]) throws Exception {
        BufferedReader inFromUser = new BufferedReader(
                new InputStreamReader(System.in)
        );
        try (DatagramSocket clientSocket = new DatagramSocket()) {
            InetAddress IPAddress = InetAddress.getByName("localhost");

            byte[] sendData1k = ("OI").getBytes();

            //String sentence = inFromUser.readLine();
            // 1k
            DatagramPacket sendPacket = new DatagramPacket(
                    sendData1k,
                    sendData1k.length,
                    IPAddress,
                    2716
            );

            clientSocket.send(sendPacket);
            System.out.println("Enviado " + new String(sendData1k) + ", recebendo arquivo");

            DatagramSocket serverSocket = new DatagramSocket(2717);
            byte[] receiveData = new byte[1024 * 64];
            DatagramPacket receivePacket = new DatagramPacket(receiveData,
                    receiveData.length
            );

            serverSocket.receive(receivePacket);

            System.out.println("Recieved: " + new String(receivePacket.getData()));

        }
    }

}
