/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.file.Files;

/**
 *
 * @author gustavo
 */
public class ServidorUDP {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String args[]) throws Exception {
        DatagramSocket serverSocket = new DatagramSocket(2716);
        byte[] receiveData = new byte[1024 * 64];
        while (true) {
            DatagramPacket receivePacket = new DatagramPacket(receiveData,
                    receiveData.length
            );

            serverSocket.receive(receivePacket);

            InetAddress ipcli = receivePacket.getAddress();

            System.out.println("Found Client: " + ipcli.getHostAddress() + " at port " + receivePacket.getPort());

            String sent = new String(receivePacket.getData());
            System.out.println("He sent " + sent);

            byte[] array = Files.readAllBytes(new File("oi.txt").toPath());

            DatagramPacket sendPacket = new DatagramPacket(
                    array,
                    array.length,
                    ipcli,
                    2717
            );

            serverSocket.send(sendPacket);
            System.out.println("Sent file");
            //
        }
    }

}
