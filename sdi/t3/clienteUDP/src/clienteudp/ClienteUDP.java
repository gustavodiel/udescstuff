/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteudp;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gustavo
 */
public class ClienteUDP {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String args[]) throws Exception {
        BufferedReader inFromUser = new BufferedReader(
                new InputStreamReader(System.in)
        );
        try (DatagramSocket clientSocket = new DatagramSocket()) {
            InetAddress IPAddress = InetAddress.getByName("localhost");

            byte[] sendData1k = ("OI").getBytes();

            //String sentence = inFromUser.readLine();
            // 1k
            DatagramPacket sendPacket = new DatagramPacket(
                    sendData1k,
                    sendData1k.length,
                    IPAddress,
                    2716
            );

            clientSocket.send(sendPacket);
            System.out.println("Enviado " + new String(sendData1k) + ", recebendo arquivo");

            DatagramSocket serverSocket = new DatagramSocket(2717);
            byte[] receiveData = new byte[1024 * 64];
            DatagramPacket receivePacket = new DatagramPacket(receiveData,
                    receiveData.length
            );

            serverSocket.receive(receivePacket);

            System.out.println("Recieved: " + new String(receivePacket.getData()));

        }
    }

}

class ServidorCenChat {

    public static final int PORTServidor = 8889;
    public static final int PORTMulticast = 8888;

    public static final String IPMulticast = "224.2.2.3";
    public static final String IPServidor = "localhost";

    private DatagramSocket socketIn = null;
    private MulticastSocket socketOut = null;

    private DatagramPacket outPacket = null;
    private DatagramPacket inPacket = null;

    private BufferedReader inputLine = null;

    InetAddress addressMulticast;
    InetAddress addressServidor;

    ArrayList<String> msgs = new ArrayList<>();

    ServidorCenChat() {
        try {
            socketOut = new MulticastSocket(PORTMulticast);
            socketIn = new DatagramSocket(PORTServidor);

            addressMulticast = InetAddress.getByName(IPMulticast);
            addressServidor = InetAddress.getByName(IPServidor);

            inputLine = new BufferedReader(new InputStreamReader(System.in));
        } catch (SocketException ex) {
            Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void listenMulticast() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] inBuf = new byte[256];
                try {
                    while (true) {
                        inPacket = new DatagramPacket(inBuf, inBuf.length);
                        socketIn.receive(inPacket);
                        synchronized (msgs) {
                            String msg = new String(inBuf, 0, inPacket.getLength());

                            System.out.println("From " + inPacket.getAddress() + ":" + inPacket.getPort() + " -> " + msg);
                            msgs.add(msg);
                        }

                    }
                } catch (IOException ex) {
                    Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }).start();
    }

    private void sendMulticast() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] outBuf = new byte[256];
                
                try {
                    socketOut.joinGroup(addressMulticast);
                    synchronized (msgs) {
                        while (true) {
                            if (!msgs.isEmpty()) {
                                String msg = msgs.remove(0);
                                outBuf = msg.getBytes();

                                //Send to multicast IP address and port
                                outPacket = new DatagramPacket(outBuf, outBuf.length, addressMulticast, PORTMulticast);

                                socketOut.send(outPacket);

                                System.out.println("Enviado: " + msg);
                            }
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }).start();
    }

    public static void main(String args[]) {
        ServidorCenChat servidor = new ServidorCenChat();
        servidor.listenMulticast();
        servidor.sendMulticast();
    }
}

class ClienteCenChat {

    public static final int PORTServidor = 8889;
    public static final int PORTMulticast = 8888;

    public static final String IPMulticast = "224.2.2.3";
    public static final String IPServidor = "localhost";

    private MulticastSocket socketIn = null;
    private DatagramSocket socketOut = null;

    private DatagramPacket outPacket = null;
    private DatagramPacket inPacket = null;

    private BufferedReader inputLine = null;

    InetAddress addressMulticast;
    InetAddress addressServidor;

    ClienteCenChat() {
        try {
            socketOut = new DatagramSocket();
            socketIn = new MulticastSocket(PORTMulticast);

            addressMulticast = InetAddress.getByName(IPMulticast);
            addressServidor = InetAddress.getByName(IPServidor);

            inputLine = new BufferedReader(new InputStreamReader(System.in));
        } catch (SocketException ex) {
            Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void listenMulticast() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] inBuf = new byte[256];
                try {
                    socketIn.joinGroup(addressMulticast);
                    while (true) {
                        inPacket = new DatagramPacket(inBuf, inBuf.length);
                        socketIn.receive(inPacket);
                        if (socketOut.getLocalPort() != inPacket.getPort()) {
                            String msg = new String(inBuf, 0, inPacket.getLength());

                            System.out.println("From " + inPacket.getAddress() + ":" + inPacket.getPort() + " -> " + msg);
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }).start();
    }

    private void sendMulticast() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] outBuf = new byte[256];
                try {

                    while (true) {
                        String msg = inputLine.readLine();
                        outBuf = msg.getBytes();

                        //Send to multicast IP address and port
                        outPacket = new DatagramPacket(outBuf, outBuf.length, addressServidor, PORTServidor);

                        socketOut.send(outPacket);

                        System.out.println("Enviado: " + msg);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }).start();
    }

    public static void main(String args[]) {
        ClienteCenChat cliente = new ClienteCenChat();
        cliente.listenMulticast();
        cliente.sendMulticast();
    }
}

class ClienteDistChat {

    public static final int PORT = 8888;
    public static final String IP = "224.2.2.3";

    private MulticastSocket socketIn = null;
    private DatagramSocket socketOut = null;

    private DatagramPacket outPacket = null;
    private DatagramPacket inPacket = null;

    private BufferedReader inputLine = null;

    InetAddress address;

    ClienteDistChat() {
        try {
            socketOut = new DatagramSocket();
            socketIn = new MulticastSocket(PORT);
            address = InetAddress.getByName(IP);
            inputLine = new BufferedReader(new InputStreamReader(System.in));
        } catch (SocketException ex) {
            Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void listenMulticast() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] inBuf = new byte[256];
                try {
                    socketIn.joinGroup(address);
                    while (true) {
                        inPacket = new DatagramPacket(inBuf, inBuf.length);
                        socketIn.receive(inPacket);
                        if (socketOut.getLocalPort() != inPacket.getPort()) {
                            String msg = new String(inBuf, 0, inPacket.getLength());

                            System.out.println("From " + inPacket.getAddress() + ":" + inPacket.getPort() + " -> " + msg);
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }).start();
    }

    private void sendMulticast() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] outBuf = new byte[256];
                try {

                    while (true) {
                        String msg = inputLine.readLine();
                        outBuf = msg.getBytes();

                        //Send to multicast IP address and port
                        outPacket = new DatagramPacket(outBuf, outBuf.length, address, PORT);

                        socketOut.send(outPacket);

                        System.out.println("Enviado: " + msg);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }).start();
    }

    public static void main(String args[]) {
        ClienteDistChat cliente = new ClienteDistChat();
        cliente.listenMulticast();
        cliente.sendMulticast();
    }
}
