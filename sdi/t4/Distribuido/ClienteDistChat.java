

import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

class ClienteDistChat {

    public static final int PORT = 8888;
    public static final String IP = "224.2.2.3";

    private MulticastSocket socketIn = null;
    private DatagramSocket socketOut = null;

    private DatagramPacket outPacket = null;
    private DatagramPacket inPacket = null;

    private BufferedReader inputLine = null;

    InetAddress address;

    ClienteDistChat() {
        try {
            socketOut = new DatagramSocket();
            socketIn = new MulticastSocket(PORT);
            address = InetAddress.getByName(IP);
            inputLine = new BufferedReader(new InputStreamReader(System.in));
        } catch (SocketException ex) {
            Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void listenMulticast() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] inBuf = new byte[256];
                try {
                    socketIn.joinGroup(address);
                    while (true) {
                        inPacket = new DatagramPacket(inBuf, inBuf.length);
                        socketIn.receive(inPacket);
                        if (socketOut.getLocalPort() != inPacket.getPort()){
                            String msg = new String(inBuf, 0, inPacket.getLength());

                            System.out.println("From " + inPacket.getAddress() + ":" + inPacket.getPort() + " -> " + msg);
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }).start();
    }

    private void sendMulticast() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] outBuf = new byte[256];
                try {

                    while (true) {
                        String msg = inputLine.readLine();
                        outBuf = msg.getBytes();

                        //Send to multicast IP address and port
                        outPacket = new DatagramPacket(outBuf, outBuf.length, address, PORT);

                        socketOut.send(outPacket);

                        System.out.println("Enviado: " + msg);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ClienteDistChat.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }).start();
    }

    public static void main(String args[]) {
        ClienteDistChat cliente = new ClienteDistChat();
        cliente.listenMulticast();
        cliente.sendMulticast();
    }
}
