
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

class ServidorCenChat {

    public static final int PORTServidor = 8899;
    public static final int PORTMulticast = 8898;

    public static final String IPMulticast = "224.2.2.3";
    public static final String IPServidor = "localhost";

    private DatagramSocket socketIn = null;
    private MulticastSocket socketOut = null;

    private DatagramPacket outPacket = null;
    private DatagramPacket inPacket = null;

    private BufferedReader inputLine = null;

    InetAddress addressMulticast;
    InetAddress addressServidor;

    ArrayList<String> msgs = new ArrayList<>();

    ServidorCenChat() {
        try {
            socketOut = new MulticastSocket(PORTMulticast);
            socketIn = new DatagramSocket(PORTServidor);

            addressMulticast = InetAddress.getByName(IPMulticast);
            addressServidor = InetAddress.getByName(IPServidor);

            inputLine = new BufferedReader(new InputStreamReader(System.in));
        } catch (SocketException ex) {
            Logger.getLogger(ServidorCenChat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ServidorCenChat.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void listenMulticast() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] inBuf = new byte[256];
                try {
                    while (true) {
                        inPacket = new DatagramPacket(inBuf, inBuf.length);
                        socketIn.receive(inPacket);
                        synchronized (msgs) {
                            String msg = new String(inBuf, 0, inPacket.getLength());

                            System.out.println("From " + inPacket.getAddress() + ":" + inPacket.getPort() + " -> " + msg);
                            msgs.add(msg);
                        }

                    }
                } catch (IOException ex) {
                    Logger.getLogger(ServidorCenChat.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }).start();
    }

    private void sendMulticast() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] outBuf = new byte[256];

                try {
                    socketOut.joinGroup(addressMulticast);
                    synchronized (msgs) {
                        while (true) {
                            if (!msgs.isEmpty()) {
                                String msg = msgs.remove(0);
                                outBuf = msg.getBytes();

                                //Send to multicast IP address and port
                                outPacket = new DatagramPacket(outBuf, outBuf.length, addressMulticast, PORTMulticast);

                                socketOut.send(outPacket);

                                System.out.println("Enviado: " + msg);
                            }
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ServidorCenChat.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }).start();
    }

    public static void main(String args[]) {
        ServidorCenChat servidor = new ServidorCenChat();
        servidor.listenMulticast();
        servidor.sendMulticast();
    }
}
