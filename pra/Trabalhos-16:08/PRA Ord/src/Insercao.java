import java.util.logging.Logger;

/**
 * Classe que implementa o algoritimo Inser��o de ordena��o por Inser��o
 * 
 * @author Adriano e Diel
 */
public class Insercao implements ISorteador {

	private static final Logger LOGGER = Logger.getLogger(Insercao.class.getName());

	private static final int BIGGER;

	static {
		BIGGER = 1;
	}

	private Integer[] tab;

	public Insercao(Integer[] tab) {
		this.tab = tab;
	}

	@Override
	public void Sort() {
		LOGGER.info("Inicio");
		int piv;
		int j;
		for (int i = 1; i < tab.length; ++i) {
			piv = tab[i];
			j = i - 1;
			while ((j >= 0) && tab[j].compareTo(piv) == BIGGER) {
				tab[j + 1] = tab[j];
				j = j - 1;
			}
			tab[j + 1] = piv;
		}
		LOGGER.info("Final");

	}

	@Override
	public void ShowElementos() {
		for (Integer integer : tab) {
			System.out.println(integer);
		}
	}
}
