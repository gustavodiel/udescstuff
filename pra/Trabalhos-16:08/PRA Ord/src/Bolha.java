import java.util.logging.Logger;

/**
 * Classe que implementa o algoritimo de ordena��o Bolha
 * 
 * @author Adriano e Diel
 *
 */
public class Bolha implements ISorteador {

	private static final Logger LOGGER = Logger.getLogger(Bolha.class.getName());

	private static final int SMALLER;

	static {
		SMALLER = -1;
	}

	private Integer[] tab;

	/**
	 * Inicializador da classe
	 * 
	 * @param tab
	 *            um vetor que ir� ser ordenado com o algoritmo Bolha.
	 */
	public Bolha(Integer[] tab) {
		this.tab = tab;
	}

	/**
	 * Funcao que ordena o vetor que foi passado no Inicializador.
	 */
	@Override
	public void Sort() {
		LOGGER.info("Inicio\n");
		for (int i = 0; i < tab.length; ++i) {
			for (int k = 0; k < tab.length; ++k) {
				if (tab[i].compareTo(tab[k]) == SMALLER) {
					int aux = tab[i];
					tab[i] = tab[k];
					tab[k] = aux;
				}
			}
		}
		LOGGER.info("Final\n");

	}

	/**
	 * Mostra todos os elementos do vetor.
	 */
	@Override
	public void ShowElementos() {
		for (Integer integer : tab) {
			System.out.println(integer);
		}
	}

}
