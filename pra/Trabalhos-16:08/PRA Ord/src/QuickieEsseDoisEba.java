import java.util.logging.Logger;

/**
 * Classe que implementa o algoritimo de ordena��o Quick Sort
 * 
 * @author Adriano e Diel
 *
 */
public class QuickieEsseDoisEba implements ISorteador {

	private static final Logger LOGGER = Logger.getLogger(QuickieEsseDoisEba.class.getName());

	private static final int SMALLER;

	static {
		SMALLER = -1;
	}

	private Integer[] tab;

	public QuickieEsseDoisEba(Integer[] tab) {
		this.tab = tab;
	}

	@Override
	public void Sort() {

		LOGGER.info("Inicio");
		quick(0, tab.length - 1);
		LOGGER.info("Final");

	}

	private void quick(int esq, int dir) {
		int piv = esq;
		int j;
		int aux;
		for (int i = piv + 1; i <= dir; ++i) {
			j = i;
			if (tab[j].compareTo(tab[piv]) == SMALLER) {
				aux = tab[j];
				while (j > piv) {
					tab[j] = tab[j - 1];
					j--;
				}
				tab[j] = aux;
				piv++;
			}
		}

		if (piv - 1 >= esq) {
			quick(esq, piv - 1);
		}
		if (piv + 1 <= dir) {
			quick(piv + 1, dir);
		}
	}

	@Override
	public void ShowElementos() {
		for (Integer integer : tab) {
			System.out.println(integer);
		}
	}

}
