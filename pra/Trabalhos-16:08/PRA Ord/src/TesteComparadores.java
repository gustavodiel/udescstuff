import static org.junit.Assert.*;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Teste dos comparadores implementados na sala de aula :)
 * 
 * 
 * 
 */
public class TesteComparadores {

	private static final int TAMANHO = 100000;

	ISorteador sortBolha, sortSelecao, sortInsercao, sortShell, sortQuickSort;

	Integer[] arrayBolha = new Integer[TAMANHO], arraySelecao = new Integer[TAMANHO],
			arrayInsercao = new Integer[TAMANHO], arrayShell = new Integer[TAMANHO],
			arrayQuickSort = new Integer[TAMANHO], arrayExemplo = new Integer[TAMANHO];

	@Before
	public void setUp() throws Exception {

		SecureRandom secureRandom1 = null;

		try {
			SecureRandom semente = SecureRandom.getInstance("SHA1PRNG");
			byte[] randBytes = new byte[128];
			semente.nextBytes(randBytes);
			int seedByteCount = 5;
			byte[] seed = semente.generateSeed(seedByteCount);
			secureRandom1 = SecureRandom.getInstance("SHA1PRNG");
			secureRandom1.setSeed(seed);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		int coisa = TAMANHO;
		while (coisa-- > 0) {
			arrayExemplo[coisa] = secureRandom1.nextInt(TAMANHO);
		}

		arrayBolha = arrayExemplo.clone();
		arraySelecao = arrayExemplo.clone();
		arrayInsercao = arrayExemplo.clone();
		arrayShell = arrayExemplo.clone();
		arrayQuickSort = arrayExemplo.clone();
		Arrays.sort(arrayExemplo);
		sortBolha = new Bolha(arrayBolha);
		sortSelecao = new Bolha(arraySelecao);
		sortInsercao = new Bolha(arrayInsercao);
		sortShell = new Bolha(arrayShell);
		sortQuickSort = new Bolha(arrayQuickSort);

	}

	@After
	public void tearDown() throws Exception {
		sortBolha = null;
		sortSelecao = null;
		sortInsercao = null;
		sortShell = null;
		sortQuickSort = null;
		
		arrayBolha = null;
		arraySelecao = null;
		arrayInsercao = null;
		arrayShell = null;
		arrayQuickSort = null;
		arrayExemplo = null;
	}

	@Test
	public void testBolha() {
		sortBolha.Sort();
		assertArrayEquals(arrayExemplo, arrayBolha);
	}

	@Test
	public void testSelecao() {
		sortSelecao.Sort();
		assertArrayEquals(arrayExemplo, arraySelecao);
	}

	@Test
	public void testInsercao() {
		sortInsercao.Sort();
		assertArrayEquals(arrayExemplo, arrayInsercao);
	}

	@Test
	public void testShell() {
		sortShell.Sort();
		assertArrayEquals(arrayExemplo, arrayShell);
	}

	@Test
	public void testQuickSort() {
		sortQuickSort.Sort();
		assertArrayEquals(arrayExemplo, arrayQuickSort);
	}

}
