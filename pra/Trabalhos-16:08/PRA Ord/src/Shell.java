import java.util.logging.Logger;

/**
 * Classe que implementa o algoritimo de ordena��o Shell
 * 
 * @author Adriano e Diel
 *
 */
public class Shell implements ISorteador {

	private static final Logger LOGGER = Logger.getLogger(Shell.class.getName());

	private static final int BIGGER;

	static {
		BIGGER = 1;
	}

	private Integer[] tab;

	public Shell(Integer[] tab) {
		this.tab = tab;
	}

	@Override
	public void Sort() {
		int piv = 1;
		LOGGER.info("Inicio");
		while (piv < tab.length) {
			piv = piv * 3 + 1;
		}
		piv = piv / 3;
		int c, j;
		while (piv > 0) {
			for (int i = piv; i < tab.length; i++) {
				c = tab[i];
				j = i;
				while (j >= piv && tab[j - piv].compareTo(c) == BIGGER) {
					tab[j] = tab[j - piv];
					j = j - piv;
				}
				tab[j] = c;
			}
			piv = piv / 2;
		}
		LOGGER.info("Final");

	}

	@Override
	public void ShowElementos() {
		for (Integer integer : tab) {
			System.out.println(integer);
		}
	}
}
