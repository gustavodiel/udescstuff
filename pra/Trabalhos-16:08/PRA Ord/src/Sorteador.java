import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Classe principal para executar os algoritmos criados em sala.
 * 
 * @author udesc
 *
 */
public class Sorteador {

	private static final int TAMANHO = 100000;

	private static ISorteador sort;

	public static void main(String[] args) {

		SecureRandom secureRandom1 = null;
		Integer[] oi = new Integer[TAMANHO];
		
		try {
			SecureRandom semente = SecureRandom.getInstance("SHA1PRNG");
			byte[] randBytes = new byte[128];
			semente.nextBytes(randBytes);
			int seedByteCount = 5;
			byte[] seed = semente.generateSeed(seedByteCount);
			secureRandom1 = SecureRandom.getInstance("SHA1PRNG");
			secureRandom1.setSeed(seed);
		} catch (NoSuchAlgorithmException e) {

		}
		int coisa = TAMANHO;
		while (coisa-- > 0) {
			oi[coisa] = secureRandom1.nextInt(TAMANHO);
		}

		sort = new Selecao(oi);

		executarAlgoritmo();

	}

	private static void executarAlgoritmo() {
		sort.Sort();
		sort.ShowElementos();

	}

}
