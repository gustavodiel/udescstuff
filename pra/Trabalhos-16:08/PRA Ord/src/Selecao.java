import java.util.logging.Logger;

/**
 * Classe que implementa o algoritimo de ordena��o por Sele��o
 * 
 * @author Adriano e Diel
 * 
 */
public class Selecao implements ISorteador {

	private static final Logger LOGGER = Logger.getLogger(Selecao.class.getName());

	private static final int SMALLER;

	static {
		SMALLER = -1;
	}

	private Integer[] tab;

	public Selecao(Integer[] tab) {
		this.tab = tab;
	}

	@Override
	public void Sort() {

		int min, aux;
		LOGGER.info("Inicio\n");
		for (int i = 0; i < tab.length; ++i) {
			min = i;
			for (int j = i + 1; j < tab.length; ++j) {
				if (tab[j].compareTo(tab[min]) == SMALLER) {
					min = j;

				}
			}

			if (i != min) {
				aux = tab[i];
				tab[i] = tab[min];
				tab[min] = aux;
			}
		}
		LOGGER.info("Final\n");

	}

	@Override
	public void ShowElementos() {
		for (Integer integer : tab) {
			System.out.println(integer);
		}
	}

}
