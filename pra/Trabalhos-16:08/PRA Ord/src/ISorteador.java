/**
 * Interface para as classes de ordena��o
 * 
 * @author Adriano e Diel
 *
 */
public interface ISorteador {

	public void Sort();

	public void ShowElementos();
}
