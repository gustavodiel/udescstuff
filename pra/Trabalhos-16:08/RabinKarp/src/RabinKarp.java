import java.security.SecureRandom;

/**
 * Classe Rabin-Karp para procurar strings em textos.
 * 
 * @author Adriano e Gustavo
 *
 */
public class RabinKarp {
	
	private String padrao;		// Padrao a ser procurado
	private long hashPadrao;	// Hash do padrao
	private int sizePadrao;		// Comprimento do padrao
	
	private String texto;		// Texto que contem o padrao (ou nao)
	
	/**
	 * Inicializador da classe
	 * @param padrao Palavra a ser encontrada
	 */
	public RabinKarp(String padrao){
		this.padrao = padrao;
		this.sizePadrao = padrao.length();
		this.hashPadrao = CalculaHash(padrao);
	}
	
	/**
	 * Nem cheguei a utilizar, mas se o cara quisar mudar a palavra de busca sem criar uma outra instancia, utiliza-se este metodo.
	 * @param padrao nova palavra de busca
	 */
	public void MudaPadrao(String padrao){
		this.padrao = padrao;
		this.sizePadrao = padrao.length();
		this.hashPadrao = CalculaHash(padrao);
	}
	
	/**
	 * Aqui acontece a magica do algortimo, onde procuramos a palavra baseada numa hash. Se acharmos, comparamos a string com a palavra de busca.
	 * @param texto O texto que contem ou nao a palavra
	 * @return a posicao ( -1 indica que nao foi encontrado )
	 */
	public int Search(String texto){
		int pos = -1;
		for (int i = 0; i < texto.length(); ++i){
			if (i + sizePadrao > texto.length())
				break;
			String comp = texto.substring(i, i+sizePadrao);
			if (CalculaHash(comp) == hashPadrao){
				if (padrao.equals(comp)){
					pos = i;
					break;
				}
			}
		}
		return pos;
	}
	
	/**
	 * Calcula a hash de uma string, utilizando o numero 833897 ( um numero primo ) como chave para evitar repeticao e troços.
	 * 
	 * @param texto String a se criar uma hash
	 * @return A hash criada. Dur.
	 */
	private long CalculaHash(String texto){
		long primo = 833897;
		long hash = 0;
		short radical = 256;
		for (int i = 1; i < texto.length(); ++i){
			hash = (radical*hash + texto.charAt(i)) % primo;
		}
		return hash;
	}

	/**
	 * 
	 * @return a palavra a ser buscada.
	 */
	public String getPalavra() {
		return padrao;
	}
	
}
