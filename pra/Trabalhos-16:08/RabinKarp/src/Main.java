import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JOptionPane;
/**
 * 
 * Classe principal para executar o codigo de busca de uma palavra em um texto .txt utilizando o algortimo de RabinKarp.
 * 
 * @author Adriano e Gustavo a.k.a. Diel
 * 
 */
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String pal = JOptionPane.showInputDialog("Por favor digite a palavra que desejas encontrar");
		
		RabinKarp search = new RabinKarp(pal);
		
		execTeste(search);
	}

	private static void execTeste(RabinKarp search) {
		try{
			FileReader arq = new FileReader("lerolero.txt");
			BufferedReader lerArq = new BufferedReader(arq);
			String linha = lerArq.readLine();
			String padrao = search.getPalavra();
			int buscas = 0;
			int numeroLinha = 1;
			
			while ( linha != null){
				int res = search.Search(linha);
				if (res!=-1){
					buscas++;
					int opt = JOptionPane.showOptionDialog(null, "Achamos a palavra " + search.getPalavra() + " na posicao " 
							+ (res + 1)+ " da linha "+numeroLinha+ ". Desejas procurar outra instancia?", "Achamos",
							JOptionPane.YES_NO_OPTION, 
							JOptionPane.QUESTION_MESSAGE,
							null,
							null,
							null);
					if (opt == JOptionPane.NO_OPTION){
						break;
					}
					
					if (linha.length() > padrao.length()){
						linha = linha.substring(res + padrao.length(), linha.length());
					} else {
						linha = lerArq.readLine();
						numeroLinha++;
					}
					
				} else {
					linha = lerArq.readLine();
					numeroLinha++;
				}
			}
			if (buscas == 0){
				JOptionPane.showMessageDialog(null, "Nao achamos a palavra desejada", "Nada", JOptionPane.ERROR_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(null, "Achamos a palavra " + buscas +" vezes", "Nada", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (IOException e){
			e.printStackTrace();
		}
		
	}

}
