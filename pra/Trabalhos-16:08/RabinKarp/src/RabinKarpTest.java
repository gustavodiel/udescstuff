import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RabinKarpTest {
	RabinKarp searcher;
	
	@Before
	public void setUp() throws Exception {
		searcher = new RabinKarp("Test");
	}

	@After
	public void tearDown() throws Exception {
		searcher = null;
	}

	@Test
	public void testPosicao() {
		int s = searcher.Search("OIe Test");
		assertEquals(s, 4);
	}

}
